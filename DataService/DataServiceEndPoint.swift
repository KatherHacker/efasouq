//
//  DataServiceEndPoint.swift
//   TeamMotivation
//
//  Created by Admin on 2/10/19.
//
import Foundation

let componentScheme = "https"
let componentHost = "digitalnarration.co.in"
let componentPort: NSNumber = 443
let componentPathSuffix = "/api"

let baseImageUrl = "https://digitalnarration.co.in:443/api/uploads/"
let baseImageUrl1 = "https://digitalnarration.co.in:443/api/uploads/"

let baseURL = "https://digitalnarration.co.in:443/api/"

var skipOTPVerification: Bool = false

enum DataServiceEndpoint: AddressableEndPoint {
    
    case register, login, uploadProfileImage, uploadBannerImage, exploreProducts, syncService, getProfile, updateProfile, createStore, createBam, addProduct, updateProduct, exploreProduct, getOtherUserInfoById, sendCollobarationRequest, getDesignerGallery, allDesigners, toggleFollow, checkModel, createPremiumProduct, createOrder, paymentStatus, fcmRegister, allDesignerNames, getCollabarationRequests, likeProduct, deleteProduct, completeProfile
    
    var endPointInfo: CoreDataServiceEndPointInfo {
        get {
            switch self {
            case .completeProfile:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/completeProfile/", verb: .Put, authroizationType: .user)
            case .syncService:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/product/syncService/", verb: .Get, authroizationType: .none)
            case .register:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/register/", verb: .Post, authroizationType: .none)
            case .login:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/login/", verb: .Post, authroizationType: .none)
            case .getProfile:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/profile/", verb: .Get, authroizationType: .user)
            case .updateProfile:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/profile/update/", verb: .Put, authroizationType: .user)
            case .uploadProfileImage:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/profile/uploadProfileImage/", verb: .Post, authroizationType: .user)
            case .uploadBannerImage:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/profile/uploadBannerImage/", verb: .Post, authroizationType: .user)
            case .exploreProducts:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/product/explore", verb: .Get, authroizationType: .none)
            case .createStore:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/store/online/", verb: .Post, authroizationType: .user)
            case .createBam:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/store/bam/", verb: .Post, authroizationType: .user)
            case .addProduct:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/product/addProduct/", verb: .Post, authroizationType: .user)
            case .updateProduct:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/product/updateProduct/", verb: .Put, authroizationType: .user)
            case .exploreProduct:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/product/explore/", verb: .Get, authroizationType: .user)
            case .getOtherUserInfoById:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/", verb: .Get, authroizationType: .user)
            case .sendCollobarationRequest:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/collab/sendCollabReq/", verb: .Post, authroizationType: .user)
            case .getDesignerGallery:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/designerGallery/", verb: .Post, authroizationType: .user)
            case .allDesigners:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/designer/", verb: .Get, authroizationType: .user)
            case .toggleFollow:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/follow/", verb: .Post, authroizationType: .user)
            case .checkModel:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/product/checkModel/", verb: .Post, authroizationType: .user)
            case .createPremiumProduct:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/order/addDigitalProduct/", verb: .Post, authroizationType: .user)
            case .createOrder:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/order/createOrder/", verb: .Post, authroizationType: .user)
            case .paymentStatus:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/order/payment_status/", verb: .Post, authroizationType: .user)
            case .fcmRegister:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/user/fcmRegister/", verb: .Post, authroizationType: .user)
            case .allDesignerNames:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/designer/names/", verb: .Get, authroizationType: .user)
            case .getCollabarationRequests:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/collab/getCollabReq/", verb: .Get, authroizationType: .user)
            case .likeProduct:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/like/", verb: .Post, authroizationType: .user)
            case .deleteProduct:
                return CoreDataServiceEndPointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/product/deleteProduct/", verb: .Delete, authroizationType: .user)
            }
        }
    }
    
    static func urlFromEndPoint(_ endPoint: DataServiceEndpoint) -> URL? {
        return URL.init(string: DataServiceEndpoint.urlStringFromEndPoint(endPoint))
    }
    
    static func urlStringFromEndPoint(_ endPoint: DataServiceEndpoint) -> String {
        let endPointInfo = endPoint.endPointInfo
        var urlStr = endPointInfo.scheme+"://"+endPointInfo.host
        if endPointInfo.port != 0 {
            urlStr += ":\(endPointInfo.port)/"
        }
        urlStr += endPointInfo.path
        return urlStr
    }
}
