//
//  DataService.swift
//   TeamMotivation
//
//  Created by Admin on 2/10/19.

//

import Foundation
import UIKit

class DeserializationError: CoreError {
    init() {
        super.init(code: 1002, description: "Deserialization error", innerError: nil, informations: nil)
    }
}

class DataService: CoreDataService {
    
    override init() {
        super.init()
        self.userAutheticateToken = CacheManager.sharedInstance.getObjectForKey(.AuthToken) as AnyObject
    }
    
    var contentType: RequestContentType = .json
    
    override var requestContentType: RequestContentType {
        return self.contentType
    }
    
    override func requestEndPoint(_ endPoint: AddressableEndPoint, withParameters parameters: AnyObject?, withBody body: AnyObject?, withHeaders headers: AnyObject?, andPathSuffix pathSuffix: AnyObject?, onCompletion completion: @escaping (() throws -> Data?, AnyObject?) -> Void) {
        var newHeaders = headers as? [String: Any] ?? [String: Any]()
//        newHeaders["device-type"] = "ios"
        super.requestEndPoint(endPoint, withParameters: parameters, withBody: body, withHeaders: newHeaders as AnyObject?, andPathSuffix: pathSuffix) { (inner, header) in
            do {
                let data = try inner()
                let commonRes = data?.getDecodedObject(from: CommonRes.self)
//                (UIApplication.shared.delegate as? AppDelegate)?.keyWindow?.showToast(commonRes?.message)
//                    completion(inner, header)
            } catch _ as UnAuthorisedError {
                if CacheManager.sharedInstance.getObjectForKey(.AuthToken) != nil {
                    NotificationCenter.default.post(name: NSNotification.Name("UNAUTHENTICATED"), object: nil)
                    self.forceLogout()
                }
            } catch let err1 as TimeOutError {
                self.handleTimeOut
                completion({ throw err1 }, nil)
            } catch let err as NetWorkNotReachError {
                self.handleNetworkNotReach
                completion({ throw err }, nil)
            } catch let error as BadRequestError {
                print(error)
                completion({ throw error }, nil)
            } catch {
                completion({ throw error }, nil)
            }
        }
    }
    
//    override func uploadImages(_ info: [String : UIImage], endPoint: AddressableEndPoint, withBodyParameters parameters: AnyObject? = nil, withHeaders headers: AnyObject? = nil, andPathSuffix pathSuffix: AnyObject? = nil, mimeType: MimeType = .jpeg, onCompletion completion: @escaping (() throws -> Data?) -> Void) {
//        super.uploadImages(info, endPoint: endPoint, withBodyParameters: parameters, withHeaders: headers, andPathSuffix: pathSuffix, mimeType: mimeType) { (inner) in
//            do {
//                let data = try inner()
//                let commonRes = data?.getDecodedObject(from: CommonRes.self)
//                (UIApplication.shared.delegate as? AppDelegate)?.keyWindow?.showToast(commonRes?.message)
//                completion({ return data })
//            } catch _ as UnAuthorisedError {
//                if CacheManager.sharedInstance.getObjectForKey(.AuthToken) != nil {
//                    NotificationCenter.default.post(name: NSNotification.Name("UNAUTHENTICATED"), object: nil)
//                    self.forceLogout()
//                }
//            } catch let err1 as TimeOutError {
//                self.handleTimeOut
//                completion({ throw err1 })
//            } catch let err as NetWorkNotReachError {
//                self.handleNetworkNotReach
//                completion({ throw err })
//            } catch let error as BadRequestError {
//                print(error)
//                completion({ throw error })
//            } catch {
//                completion({ throw error })
//            }
//        }
//    }
//
}

extension NSObject {
    var handleTimeOut: Void {
        UIApplication.shared.windows.first?.rootViewController?.stopActivityIndicator()
        UIApplication.shared.windows.first?.rootViewController?.vcToPresent?.showAlertMessage("Request timed out, please try again.", title: "Request Timeout")
        return
    }
    
    var handleSessionExpire: Void {
        UIApplication.shared.windows.first?.rootViewController?.stopActivityIndicator()
        UIApplication.shared.windows.first?.rootViewController?.vcToPresent?.showAlertWith("Session expired, please login again.", title: "", completion: { [weak self] (style) -> (Void) in
            self?.forceLogout()
        })
        return
    }
    
    var handleNetworkNotReach: Void {
        UIApplication.shared.windows.first?.rootViewController?.stopActivityIndicator()
        UIApplication.shared.windows.first?.rootViewController?.vcToPresent?.showAlertMessage("Please make sure you are connected to the internet.", title: "Network Connection Failed")
        return
    }
}

extension NSObject {
    func forceLogout() {
        CacheManager.sharedInstance.clearUserData()
        Router.setAuthNavigationControllerAsRoot()
    }
}

extension DataService {

    func login(_ info: LoginReq, completion: ((_ result: UserInfo?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.login, withParameters:nil, withBody: info.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: UserInfo.self)
                CacheManager.sharedInstance.setObject(response?.token?.stringValue ?? "", key: .AuthToken)
                CacheManager.userInfo = response
                CacheManager.sharedInstance.saveCache()
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }

    func signup(_ info: RegisterReq, completion: ((_ result: UserInfo?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .formurlencoded
        self.requestEndPoint(DataServiceEndpoint.register, withParameters:nil, withBody: info.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponseSingle<UserInfo>.self)
                CacheManager.sharedInstance.setObject(response?.data?.token?.stringValue ?? "", key: .AuthToken)
                CacheManager.userInfo = response?.data
                CacheManager.sharedInstance.saveCache()
                completion?(response?.data, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
//    func designerOnboardInfoSync(_ completion: ((_ result: DesignerOnboardInfo?, _ error: Error?) -> Void)? = nil) {
//        self.contentType = .json
//        self.requestEndPoint(DataServiceEndpoint.syncService, withParameters:nil, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
//            do {
//                let data = try inner()
//                let response = data?.getDecodedObject(from: DesignerOnboardInfo.self)
//                completion?(response, nil)
//            } catch {
//                completion?(nil, error)
//            }
//        }
//    }
    
    func getProfile(_ completion: ((_ result: ProfileInfo?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .json
        self.requestEndPoint(DataServiceEndpoint.getProfile, withParameters:nil, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: ProfileInfo.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func updateProfile(_ req: ProfileReq, completion: ((_ result: ProfileInfo?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .multipartformdata
//        self.uploadImages(req.images, endPoint: DataServiceEndpoint.updateProfile, withBodyParameters: req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil, mimeType: .png) { (inner) in
//            do {
//                let data = try inner()
//                let response = data?.getDecodedObject(from: ProfileInfo.self)
//                completion?(response, nil)
//            } catch {
//                completion?(nil, error)
//            }
//        }
        self.requestEndPoint(DataServiceEndpoint.updateProfile, withParameters:nil, withBody: req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: ProfileInfo.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func completeProfile(_ req: CompleteProfileReq, completion: ((_ result: CompleteProfileRes?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .multipartformdata
        self.uploadImageOrFile(UIImage(), filePathKey: "image", endPoint: DataServiceEndpoint.completeProfile, withBodyParameters: req.JSONRepresentation as AnyObject?, mimeType: .png) { (inner) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponseSingle<CompleteProfileRes>.self)
                completion?(response?.data, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func uploadProfileImage(_ image: UIImage, completion: ((_ result:  GeneralResponseSingle<UploadInfo>?, _ error: Error?) -> Void)? = nil) {
        self.uploadImageOrFile(image, filePathKey: "image", endPoint: DataServiceEndpoint.uploadProfileImage, withBodyParameters: nil, mimeType: .png) { (inner) in
            do {
                let data = try inner()
                let result = data?.getDecodedObject(from: GeneralResponseSingle<UploadInfo>.self)
                completion?(result, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func uploadBannerImage(_ image: UIImage, completion: ((_ result:  GeneralResponseSingle<UploadInfo>?, _ error: Error?) -> Void)? = nil) {
        self.uploadImageOrFile(image, filePathKey: "bannerImage", endPoint: DataServiceEndpoint.uploadBannerImage, withBodyParameters: nil, mimeType: .png) { (inner) in
            do {
                let data = try inner()
                let result = data?.getDecodedObject(from: GeneralResponseSingle<UploadInfo>.self)
                completion?(result, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func products(_ completion: ((_ result: Products?, _ error: Error?) -> (Void))? = nil) {
        self.requestEndPoint(DataServiceEndpoint.exploreProducts, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: Products.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func createStore(_ req: CreateStoreReq, completion: ((_ result: StoreInfo?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .multipartformdata
        self.uploadImages(req.images, endPoint: DataServiceEndpoint.createStore, withBodyParameters: req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil, mimeType: .png) { (inner) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponseSingle<StoreInfo>.self)
                completion?(response?.data, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func createBam(_ req: CreateBamReq, completion: ((_ result: StoreInfo?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .multipartformdata
        self.uploadImages(req.images, endPoint: DataServiceEndpoint.createBam, withBodyParameters: req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil, mimeType: .png) { (inner) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponseSingle<StoreInfo>.self)
                completion?(response?.data, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func addProduct(_ req: StoreProductReq, completion: ((_ result: StoreProduct?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .multipartformdata
        self.uploadImages(req.images, endPoint: DataServiceEndpoint.addProduct, withBodyParameters: req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil, mimeType: .png) { (inner) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponseSingle<StoreProduct>.self)
                completion?(response?.data, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func updateProduct(_ req: StoreProductReq, completion: ((_ result: StoreProduct?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .multipartformdata
        self.uploadImages(req.images, endPoint: DataServiceEndpoint.updateProduct, withBodyParameters: req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil, mimeType: .png) { (inner) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponseSingle<StoreProduct>.self)
                completion?(response?.data, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func exploreProduct(_ req: ExploreProductReq, completion: ((_ result: ProductExploreRes?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.exploreProduct, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: req.pathSuffix as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: ProductExploreRes.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func getOtherUserInfoById(_ req: String, completion: ((_ result: UserOtherInfo?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.getOtherUserInfoById, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: (req + "/") as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: UserOtherInfo.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }

    func sendCollabartionRequest(_ req: CollabarationReq, completion: ((_ result: CollabarationRequestRes?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.sendCollobarationRequest, withParameters:nil, withBody:req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: req.pathSuffix) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CollabarationRequestRes.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func getDesignerGallery(_ req: GetDesignerGalleryReq, completion: ((_ result: CustomType?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.getDesignerGallery, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: req.pathSuffix) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CustomType.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func checkModel(_ req: CheckModelReq, completion: ((_ result: CustomType?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.checkModel, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CustomType.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func getDesigners(_ req: GetDesignersReq, completion: ((_ result: GeneralResponseArray<DesignerInfo>?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.allDesigners, withParameters:req.JSONRepresentation as AnyObject?, withBody:nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponseArray<DesignerInfo>.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func createPremiumProduct(_ req: CreatePremiumProductReq, completion: ((_ result: CustomType?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.createPremiumProduct, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CustomType.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func createOrder(_ req: CreateOrderReq, completion: ((_ result: CreateOrderRes?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.createOrder, withParameters:req.JSONRepresentation as AnyObject?, withBody:nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CreateOrderRes.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func followUser(_ req: FollowUserReq, completion: ((_ result: CustomType?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.toggleFollow, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: req.pathSuffix) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CustomType.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func paymentStatus(_ req: PaymentStatusReq, completion: ((_ result: CustomType?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.paymentStatus, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CustomType.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func fcmRegister(_ req: FCMRegisterReq, completion: ((_ result: CustomType?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .multipartformdata
        self.requestEndPoint(DataServiceEndpoint.fcmRegister, withParameters:nil, withBody:req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CustomType.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func getAllDesignerNames(_ completion: ((_ result: GeneralResponseArray<NameInfo>?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.allDesignerNames, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponseArray<NameInfo>.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func getCollabartionRequests(_ completion: ((_ result: CollabarationRequestList?, _ error: Error?) -> Void)? = nil) {
        self.requestEndPoint(DataServiceEndpoint.allDesignerNames, withParameters:nil, withBody:nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CollabarationRequestList.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func likeProduct(_ req: LikeProductReq, completion: ((_ result: CustomType?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .multipartformdata
        self.requestEndPoint(DataServiceEndpoint.likeProduct, withParameters:nil, withBody:req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CustomType.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
    
    func deleteProduct(_ req: DeleteProductReq, completion: ((_ result: CustomType?, _ error: Error?) -> Void)? = nil) {
        self.contentType = .multipartformdata
        self.requestEndPoint(DataServiceEndpoint.deleteProduct, withParameters:nil, withBody:req.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: req.pathSuffix) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CustomType.self)
                completion?(response, nil)
            } catch {
                completion?(nil, error)
            }
        }
    }
}

struct DeleteProductReq {
    var productId: String!
    var productType: String!
    var JSONRepresentation: [String: Any] {
        return [
            "productType": self.productType ?? "",
        ]
    }
    
    var pathSuffix: AnyObject? {
        return ((self.productId ?? "") + "/") as AnyObject
    }
}

struct LikeProductReq: JSONSerializable {
    var productId: String!
    var subProductId: String!
    var userId: String!
}

struct CollabarationRequestList: JSONSerializable {
    let sent, received: [CollabarationRequestInfo]?
}

struct CollabarationRequestInfo: JSONSerializable {
    let to: CustomType?
    let message: CustomType?
}

struct NameInfo: JSONSerializable {
    let first_name, last_name: CustomType?
}

struct FCMRegisterReq: JSONSerializable {
    var fcmToken: String!
}

struct PaymentStatusReq: JSONSerializable {
    
}

struct CreateOrderReq: JSONSerializable {
    var orderItems, paymentMethod, taxPrice, shippingPrice, totalPrice: String!
//    orderItems:Basic Premium Plan
//    paymentMethod:RazorPay
//    taxPrice:18
//    shippingPrice:0
//    totalPrice:4999
}

struct CreateOrderRes: JSONSerializable {
    let orderDetails: OrderDetail?
}

struct OrderDetail: JSONSerializable {
    let name: CustomType?
    let description: CustomType?
    let currency: CustomType?
    let amount: CustomType?
    let phone: CustomType?
    let email: CustomType?
    let order_id: CustomType?
    let companyName: CustomType?
    let razorpay_test_keyId: CustomType?
}

struct CheckModelReq: JSONSerializable {
    
}

struct CreatePremiumProductReq: JSONSerializable {
    
}

struct FollowUserReq: JSONSerializable {
    var userid: String!
    
    var pathSuffix: AnyObject? {
        return ((self.userid ?? "") + "/") as AnyObject
    }
}

struct GetDesignersReq: JSONSerializable {
    var page, first_name, last_name, location, estb, occassion, gender, product: String!
}

struct DesignerInfo: JSONSerializable {
    var username, id, occupation, image: CustomType?
}

/* GetDesignerGallery Response
{
    "Profile": [
        {
            "bio": "I am a fashion designer based out of coimbatore .",
            "image": "https://designergallery-dev.s3.amazonaws.com/profile_pics/cropped2279893088793213510.jpg",
            "products": "{\"0\": \"Accessory\", \"1\": \"Bag\", \"2\": \"Clothing\", \"3\": \"Footwear\", \"4\": \"False\", \"5\": \"About Us \"}"
        }
    ],
    "Accessory": [
        {
            "productId": 18,
            "productType_id": 1,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/accessoryProducts/46744719TR_16_d_3OKrJuD.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/accessoryProducts/46744719TR_16_e_N43onCp.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/accessoryProducts/46744719TR_16_f_6S8srNB.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/accessoryProducts/46744719TR_16_e_KOAiWgx.jpg"
        },
        {
            "productId": 22,
            "productType_id": 1,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/accessoryProducts/Screenshot_20210522-171252__01.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/accessoryProducts/Screenshot_20210522-171252__01_O3KJb4V.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/accessoryProducts/Screenshot_20210522-171252__01_xAQKCUn.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/accessoryProducts/Screenshot_20210522-171252__01_9c4sUGJ.jpg"
        }
    ],
    "Bag": [
        {
            "productId": 1,
            "productType_id": 2,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/bagProducts/45444057AQ_16_a.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/bagProducts/45444057AQ_16_d.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/bagProducts/45444057AQ_16_f.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/bagProducts/45444057AQ_16_r.jpg"
        },
        {
            "productId": 6,
            "productType_id": 2,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg"
        }
    ],
    "Clothing": [
        {
            "productId": 1,
            "productType_id": 3,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/clothingProducts/41982490AH_16_d.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/clothingProducts/41982490AH_16_r.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/clothingProducts/41982490AH_16_d_UrtryQt.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/clothingProducts/41982490AH_16_d_Txn8cZ8.jpg"
        },
        {
            "productId": 5,
            "productType_id": 3,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/clothingProducts/Screenshot_20210522-171252__01.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/clothingProducts/Screenshot_20210522-171252__01_ftX2Atq.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/clothingProducts/Screenshot_20210522-171252__01_HbDzgnb.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/clothingProducts/Screenshot_20210522-171252__01_Twqtony.jpg"
        }
    ],
    "Footwear": [
        {
            "productId": 1,
            "productType_id": 4,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/footwearProducts/11929073UC_16_d_8OIEjle.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/footwearProducts/11929073UC_16_a_hJiV1HD.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/footwearProducts/11929073UC_16_e_DpwPLOh.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/footwearProducts/11929073UC_16_f_vRVtDYQ.jpg"
        },
        {
            "productId": 7,
            "productType_id": 4,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg"
        }
    ],
    "Jewellery": [
        {
            "productId": 1,
            "productType_id": 5,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50253259JS_16_d.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50253259JS_16_d_n8HZHJi.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50253259JS_16_d_2LzVyux.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50253259JS_16_d_3Op3CnT.jpg"
        },
        {
            "productId": 5,
            "productType_id": 5,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50249313gx_14_f.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50249313gx_14_f_TsLJEnc.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50249313gx_14_f_8Ik9MAQ.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50249313gx_14_f_fA2XAFt.jpg"
        },
        {
            "productId": 6,
            "productType_id": 5,
            "frontViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50230802AV_16_f.jpg",
            "backViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50230802AV_16_r.jpg",
            "sideViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50230802AV_16_f_1.jpg",
            "arialViewUrl": "https://designergallery-dev.s3.amazonaws.com/jewelleryProducts/50230802AV_16_f_tdG863W.jpg"
        }
    ],
    "OnlineBam": [
        {
            "store_id": 34,
            "storeName": "Cult Classic",
            "storeAddress": "www.cultclassic.com",
            "logo": "https://designergallery-dev.s3.amazonaws.com/logo/Screenshot_20210513-213452__01.jpg",
            "user": 168,
            "storeCategory": 1
        },
        {
            "store_id": 35,
            "storeName": "Cult Classic Coimbatore",
            "storeAddress": "Rs Puram Coimbatore",
            "logo": "https://designergallery-dev.s3.amazonaws.com/logo/Screenshot_20210513-213452__01_RLcpN39.jpg",
            "user": 168,
            "storeCategory": 2
        },
        {
            "store_id": 47,
            "storeName": "my New onlinew store 2021",
            "storeAddress": "www.mynewonline556.com",
            "logo": null,
            "user": 168,
            "storeCategory": 1
        },
        {
            "store_id": 49,
            "storeName": "My new store in Salem  ",
            "storeAddress": "Brick And Motor ",
            "logo": null,
            "user": 168,
            "storeCategory": 2
        }
    ]
}
*/

struct GetDesignerGalleryReq {
    var designerId: String!
    
    var pathSuffix: AnyObject? {
        return ((self.designerId ?? "") + "/") as AnyObject
    }
}

struct CollabarationReq {
    var userId: String!
    var message: String!
    
    var JSONRepresentation: [String: Any] {
        return [
            "message": self.message ?? "",
        ]
    }
    
    var pathSuffix: AnyObject? {
        return ((self.userId ?? "") + "/") as AnyObject
    }
}

// MARK: - Welcome
struct CollabarationRequestRes: JSONSerializable {
    let detail: CustomType?
    let data: CollabData?
    let collaboration: CollaborationInfo?
}

struct CollaborationInfo: JSONSerializable {
    let collabFeature: CustomType?
}

// MARK: - DataClass
struct CollabData: JSONSerializable {
    let id: CustomType?
    let message, requestDateTime: CustomType?
    let userFrom, userTo, collabStatus: CustomType?
}


struct CreateStoreReq {
    var storeName, webaddress: String!
    var logo: UIImage!
    
    var JSONRepresentation: [String: Any] {
        return [
            "storeName": self.storeName ?? "",
            "webaddress": self.webaddress ?? ""
        ]
    }
    
    var images: [String: UIImage] {
        return [
            "logo": self.logo ?? UIImage()
        ]
    }
}

struct CreateBamReq {
    var storeName, location: String!
    var logo: UIImage!
    
    var JSONRepresentation: [String: Any] {
        return [
            "storeName": self.storeName ?? "",
            "location": self.location ?? ""
        ]
    }
    
    var images: [String: UIImage] {
        return [
            "logo": self.logo ?? UIImage()
        ]
    }
}

struct StoreInfo: JSONSerializable {
    let store_id: CustomType?
    let storeName: CustomType?
    let storeAddress: CustomType?
    let logo: CustomType?
    let user: CustomType?
    let storeCategory: CustomType?
}

struct Product: JSONSerializable {
    let productID: CustomType?
    let productDesc: CustomType?
    let exploreProductImage: CustomType?

    enum CodingKeys: String, CodingKey {
        case productID = "product_id"
        case productDesc, exploreProductImage
    }
}

typealias Products = [Product]

struct ExploreProductReq {
    var productType: String!
    var productId: String!
    
    var pathSuffix: String {
        return (productType ?? "") + "/" + (productId ?? "") + "/"
    }
}

struct StoreProductReq {
    
    var productColour: String!
    var gender: String!
    var category: String!
    var occassion: String!
    var productType: String!
    var productName: String!
    var productDesc: String!
    
    var frontView: UIImage!
    var backView: UIImage!
    var sideView: UIImage!
    var arialView: UIImage!
    
    var JSONRepresentation: [String: Any] {
        return [
            "productColour": self.productColour ?? "",
            "gender": self.gender ?? "",
            "category": self.category ?? "",
            "occassion": self.occassion ?? "",
            "productType": self.productType ?? "",
            "productName": self.productName ?? "",
            "productDesc": self.productDesc ?? ""
        ]
    }
    
    var images: [String: UIImage] {
        return [
            "frontView": self.frontView ?? UIImage(),
            "backView": self.backView ?? UIImage(),
            "sideView": self.sideView ?? UIImage(),
            "arialView": self.arialView ?? UIImage()
        ]
    }
}

struct StoreProduct: JSONSerializable {
    let id:  CustomType?//7,
    let designerName:  CustomType?//"Kamaleshwar Rishis",
    let productName:  CustomType?//"Sample Product Name",
    let productDesc:  CustomType?//"Sample Product Description",
    let productColour:  CustomType?//"Sample Product Color",
    let gender:  CustomType?//"Gender",
    let category:  CustomType?//"Sample Product Category",
    let occassion:  CustomType?//"Sample Product Occassion",
    let frontView:  CustomType?//"https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
    let backView:  CustomType?//"https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
    let sideView:  CustomType?//"https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
    let arialView:  CustomType?//"https://designergallery-dev.s3.amazonaws.com/defaultProductImage.jpg",
    let frontView_height:  CustomType?//392,
    let frontView_width:  CustomType?//485,
    let backView_height:  CustomType?//392,
    let backView_width:  CustomType?//485,
    let sideView_height:  CustomType?//392,
    let sideView_width:  CustomType?//485,
    let arialView_height:  CustomType?//392,
    let arialView_width:  CustomType?//485,
    let user:  CustomType?//168,
    let profile:  CustomType?//65,
    let productType:  CustomType?//4
}

struct ProductExploreRes: JSONSerializable {
    let detail: CustomType?
    let productDetail: [StoreProduct]?
    let storeDetails: [StoreInfo]?
    let liked: CustomType?
}

struct UserOtherInfo: JSONSerializable {
    let userRelationship: UserRelationship?
    let follow: Follow?
    let collaboration: Collaboration?
    let profile: [Profile]?

    enum CodingKeys: String, CodingKey {
        case userRelationship, follow, collaboration
        case profile = "Profile"
    }
}

// MARK: - Collaboration
struct Collaboration: JSONSerializable {
    let collabFeature: Bool?
}

// MARK: - Follow
struct Follow: JSONSerializable {
    let following, followRequestStatus: CustomType?
}

// MARK: - Profile
struct Profile: JSONSerializable {
    let businessName, specialization, location: CustomType?
    let estb: CustomType?
    let country, occassion, products: CustomType?
}

// MARK: - UserRelationship
struct UserRelationship: JSONSerializable {
    let follow, collab: CustomType?
}

struct CompleteProfileReq: JSONSerializable {
    var products: String!
    var occassion: String!
    var specialization: String!
    var businessName: String!
    var phoneNumber: String!
    var city: String!
    var state: String!
    var typeOfEntity: String!
}


struct CompleteProfileRes: JSONSerializable {
    let user: CustomType?
    let occupation: CustomType?
    let city: CustomType?
    let estb: CustomType?
    let bio: CustomType?
    let products: CustomType?
    let occassion: CustomType?
    let specialization: CustomType?
    let dob: CustomType?
}
