//
//  SplashViewController.swift
//  Efasouq
//
//  Created by Admin on 14/06/21.
//

import UIKit

extension StoryBoardID {
    static let SplashViewController = "SplashViewController"
}

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startTimer()
    }
    
    var timer: Timer?
    
    func startTimer() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { [weak self](timerr) in
            self?.stopTimer()
            DispatchQueue.main.async { [weak self] in
                self?.controlFlow()
            }
        })
    }
    
    func stopTimer() {
        self.timer?.invalidate()
        self.timer = nil
        
    }
    
    func controlFlow() {
        let vc = Router.main.instantiateViewController(withIdentifier: StoryBoardID.LanguageSelectViewController)
        vc.modalPresentationStyle = .overFullScreen
    }

}
