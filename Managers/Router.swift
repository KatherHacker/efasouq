//
//  Router.swift
//  //   DNDev
//
//  Created by Admin on 4/26/21.
//

import Foundation
import UIKit

class Router {
    
    static let main = UIStoryboard(name: "Main", bundle: Bundle.main)
    static let auth = UIStoryboard(name: "Auth", bundle: Bundle.main)
    
    static func setAuthNavigationControllerAsRoot() {
        let authRoot = Router.auth.instantiateInitialViewController()
        UIViewController.rootViewController = authRoot
    }
    
    static func setDesignerOnBoardViewControllerAsRoot() {
//        let authRoot = UINavigationController(rootViewController: DesignerOnboardViewController.initFromStoryBoard("Auth"))
        
//        UIViewController.rootViewController = authRoot//SlideMenuController(mainViewController: authRoot, leftMenuViewController: UserMenuViewController.initFromStoryBoard("Additional"))
    }

}

extension UIViewController {
    static var rootViewController: UIViewController? {
        get {
            (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController
        }
        set {
            (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = newValue
        }
    }
}

struct StoryBoardID {
    struct Cell {
        
    }
    struct Segue {
        
    }
}

extension NSObject {
    var rootVC: UIViewController? {
        return keyWindow?.rootViewController
    }
    
    var keyWindow: UIWindow? {
        return UIApplication.shared.windows.first
    }
    
    var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
//    func showToast(_ message: String?, position: ToastPosition = .bottom) {
//        keyWindow?.makeToast(message, position: position)
    }
//}
