//
//  ContentView.swift
//  FoodCoup
//
//  Created by Appcoup on 3/12/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @available(iOS 13.0.0, *)
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    @available(iOS 13.0.0, *)
    static var previews: some View {
        ContentView()
    }
}
