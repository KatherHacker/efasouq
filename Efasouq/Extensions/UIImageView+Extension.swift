//
//  UIImageView+Extension.swift
//  Taxi
//
//  Created by Appcoup on 2/14/19.
//  Copyright © 2019 Shamlatech. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func makeRoundedCorner(){
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.width/2
        
    }
    
}
