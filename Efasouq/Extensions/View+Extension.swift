//
//  View+Extension.swift
//  //   DNDev
//
//  Created by Admin on 2/7/19.
//  Copyright © 2021 Admin. All rights reserved.
//

import Foundation
import UIKit

struct AssociatedKeys {
    static var isRounded: UInt8 = 0
    static var isHeightRounded: UInt8 = 0
}

extension UIView {
    
    @IBInspectable
    var isRounded: Bool {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.isRounded) as? Bool else {
                return false
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.isRounded, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            if newValue {
                self.cornerRadius = bounds.width/2
            }
        }
    }
    
    @IBInspectable
    var isHeightRounded: Bool {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.isHeightRounded) as? Bool else {
                return false
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.isHeightRounded, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            if newValue {
                self.cornerRadius = bounds.height/2
            }
        }
    }
        
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    func addConstaintsToSuperview(leadingOffset: CGFloat = 0.0, topOffset: CGFloat = 0.0) {
        
        guard superview != nil else {
            return
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        
        leadingAnchor.constraint(equalTo: superview!.leadingAnchor,
                                 constant: leadingOffset).isActive = true
        
        topAnchor.constraint(equalTo: superview!.topAnchor,
                             constant: topOffset).isActive = true
    }
    
    func addConstaints(height: CGFloat, width: CGFloat) {
        
        heightAnchor.constraint(equalToConstant: height).isActive = true
        widthAnchor.constraint(equalToConstant: width).isActive = true
    }
    
    func stretchToSuperView() {
        self.addConstaintsToSuperview()
        if let sView = self.superview {
            self.addConstaints(height: sView.bounds.size.height, width: sView.bounds.size.width)
        }
    }
    
    func addCircleLayer() -> CAShapeLayer {
        let name = "CircleAnimationLayer"
        
        if let layer = self.layer.sublayers?.filter({ $0.name == name }).first {
            layer.removeFromSuperlayer()
        }

        let shapeLayer = CAShapeLayer()
        let center = CGPoint.init(x: self.bounds.width/2, y: self.bounds.height/2)
        let circularPath = UIBezierPath.init(arcCenter: center, radius: self.bounds.width/2, startAngle: -0.0001, endAngle: (2*CGFloat.pi) + 0.0001, clockwise: false)
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.fillColor = UIColor.bluish.cgColor
        shapeLayer.borderColor = UIColor.lightGray.cgColor
        shapeLayer.borderWidth = 1
        shapeLayer.lineWidth = 15
        shapeLayer.lineJoin = CAShapeLayerLineJoin.bevel
        shapeLayer.strokeEnd = 0
        shapeLayer.fillRule = CAShapeLayerFillRule.nonZero
        shapeLayer.lineCap = CAShapeLayerLineCap.square
        shapeLayer.name = name
        self.layer.addSublayer(shapeLayer)
        return shapeLayer
    }
    
    func animateCircleWithDuration(_ duration: Double) {
        /*
        let basicAnimation = CABasicAnimation.init(keyPath: "strokeEnd")
        let pollingTime: Double = (CacheManager.settings?.polling_time?.doubleValue ?? 0)
        basicAnimation.fromValue = (pollingTime - duration) / pollingTime
        basicAnimation.toValue = 1
        basicAnimation.duration = duration
        basicAnimation.speed = 1
        basicAnimation.fillMode = CAMediaTimingFillMode.removed
        basicAnimation.isRemovedOnCompletion = false
        self.addCircleLayer().add(basicAnimation, forKey: "urSoBasic")
 */
    }
    
    class func loadFromNib<T>(withName nibName: String) -> T? {
        let nib  = UINib.init(nibName: nibName, bundle: nil)
        let nibObjects = nib.instantiate(withOwner: nil, options: nil)
        for object in nibObjects {
            if let result = object as? T {
                return result
            }
        }
        return nil
    }
    
    func className() -> String {
        return String(describing: type(of: self))
    }
}
