//
//  Date+Extension.swift
//  Taxi
//
//  Created by Appcoup on 3/2/19.
//  Copyright © 2019 Shamlatech. All rights reserved.
//

import Foundation

extension Date {
    
    public func getUTCFormateDate() -> String {
        let dateFormatter = DateFormatter()
        let timeZone = TimeZone(identifier: "UTC")
        dateFormatter.timeZone = timeZone
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return dateFormatter.string(from: self)
    }

    func formatDateForDisplay(_ format: String = "yyyy-MM-dd") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    static func dateFrom(month: Int, day: Int, year: Int) -> Date? {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        return calendar.date(from: dateComponents)
    }
    
    func dateFromFormater(_ format: String) -> String? {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = format
        dateFormater.timeZone = TimeZone.current
        return dateFormater.string(from: self)
    }
    
    func nextMonth() -> Date {
        return Calendar.current.date(byAdding: .month, value: 1, to: self) ?? Date()
    }
    
    func previousMonth() -> Date {
        return Calendar.current.date(byAdding: .month, value: -1, to: self) ?? Date()
    }
}

extension Date {
    var components:DateComponents {
        let cal = Calendar.current
        return cal.dateComponents(Set([.year, .month, .day, .hour, .minute, .second, .weekday, .weekOfYear, .yearForWeekOfYear]), from: self)
    }
}

typealias MonthDetail = (currentMonthDaysCount : Int?, weekDay: Int?, previousMonthLastDay : Int?, currentDay: Int?)

var calendar: Calendar = {
    var calendar = Calendar.current
    calendar.locale = Locale.current
    return calendar
}()

extension Date {
    
    static func monthDetailFrom( _ month: Int, year: Int = calendar.component(.year, from: Date())) -> MonthDetail {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        
        var date = Date()
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = 1
        date = calendar.date(from: dateComponents) ?? Date()
        let range = calendar.range(of: .day, in: .month, for: date)
        let components = date.components
        
        dateComponents.year = month == 1 ? year - 1 : year
        
        return (range?.count, components.weekday, date.addingTimeInterval(-60*60*24).components.day,Date().components.day)
    }
    
    func currentMonth() -> Int {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        return calendar.component(.month, from: self)
    }
    
    func currentYear() -> Int {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        return calendar.component(.year, from: self)
    }
    
    func currentDay() -> Int {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        return calendar.component(.day, from: self)
    }
    
//    func dateFromFormater(_ format: String) -> String? {
//        let dateFormater = DateFormatter()
//        //        dateFormater.locale = Locale.current
//        dateFormater.timeZone = TimeZone.current
//        dateFormater.dateFormat = format
//        return dateFormater.string(from: self)
//    }
    
    func utcDateFromFormater(_ format: String) -> String? {
        let dateFormater = DateFormatter()
        dateFormater.timeZone = TimeZone.init(identifier: "UTC")
        dateFormater.dateFormat = format
        return dateFormater.string(from: self)
    }
    
    func currentMinutes() -> Int {
        let calendar = Calendar.current
        let time=calendar.dateComponents([.minute], from: self)
        return time.value(for: .minute) ?? 0
    }
    
    func currentHour() -> Int {
        let calendar = Calendar.current
        let time=calendar.dateComponents([.hour], from: self)
        return time.value(for: .hour) ?? 0
    }
    
    func currentNormalHour() -> Int {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh" // "a" prints "pm" or "am"
        let hourString = formatter.string(from: self) // "12 AM"
        return Int(hourString) ?? 0
    }
    
    func isAm() -> Bool {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh a" // "a" prints "pm" or "am"
        let hourString = formatter.string(from: self) // "12 AM"
        return hourString.contains("am")
    }
    
    static func dateFrom(month: Int, day: Int, year: Int, minute: Int = 0) -> Date? {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.minute = minute
        return calendar.date(from: dateComponents)
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}
