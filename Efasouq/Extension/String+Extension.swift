//
//  String+Extension.swift
//  DNDev
//
//  Created by Admin on 4/27/21.
//

import Foundation

extension String {
    
    static var Empty : String {
        return ""
    }
    
    static func removeNil(_ value : String?) -> String {
        return value ?? String.Empty
    }
    
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    public func localize() -> String? {
        let language = Locale.getAppLanguage()
        let path = Bundle.main.path(forResource: language, ofType: "lproj")
        guard path != nil else {
            return self
        }
        let bundle = Bundle(path: path!)
        let string = bundle?.localizedString(forKey: self, value: nil, table: nil)
        return string!
    }
    
    func boolValue() -> Bool {
        if self == "true" || self == "True" || self == "TRUE" || self == "1" || self == "yes" || self == "YES" || self == "Yes" {
            return true
        }
        return false
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidMobileNumber() -> Bool {
        let text = self.replacingOccurrences(of: " ", with: "")
        return text.isNumber && text.count == 11
    }
    
    func isValidPassword() -> Bool {
        let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{8,}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
    
    func intValue() -> Int {
        return Int(self) ?? 0
    }
    
    func doubleValue() -> Double {
        return Double(self) ?? 0.0
    }
    
    func dateWithFormat(_ format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date = dateFormatter.date(from:self) ?? Date()
        return date
    }
    
    func UTCToLocal(_ formatFrom:String, formatTo: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatFrom
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = formatTo
        return dt != nil ? dateFormatter.string(from: dt!) : nil
    }
}
