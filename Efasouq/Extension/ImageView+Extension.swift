//
//  ImageView+Extension.swift
//  DNDev
//
//  Created by Admin on 5/16/21.
//

import Foundation
import UIKit
import Haneke

extension UIImageView {
    func loadImage(_ urlString: String?, defaultImage: UIImage? = #imageLiteral(resourceName: "Image_Unknown")) {
        let urlSttr = urlString?.hasPrefix("http") == true ? urlString : ( (urlString?.contains("uploads") == true ? baseImageUrl1 : baseImageUrl) + (urlString ?? ""))
        if let urlStr = urlSttr?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: urlStr) {
            self.hnk_setImage(from: url, placeholder: defaultImage, success: { [weak self] (image) in
                self?.image = image
                }, failure: nil)
        }
    }
}
