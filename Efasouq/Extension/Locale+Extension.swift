//
//  Locale+Extension.swift
//  DNDev
//
//  Created by Admin on 4/27/21.
//

import Foundation

public let CurrentApplicationLanguage = "CurrentApplicationLanguage"
public let AppLanguageChangedNotification = "AppLanguageChanged"

public extension Locale {
    
    static func saveAppLanguage(_ lang: String) {
        UserDefaults.standard.set(lang, forKey: CurrentApplicationLanguage)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppLanguageChangedNotification), object: nil)
    }
    
    static func getAppLanguage() -> String {
        if let lang = UserDefaults.standard.object(forKey: CurrentApplicationLanguage) as? String {
            return lang
        } else {
            return "en"
        }
    }
}

extension UserDefaults {
    static var appLanguage: String? {
        return UserDefaults.standard.value(forKey: CurrentApplicationLanguage) as? String
    }
}
