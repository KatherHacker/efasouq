//
//  TopPickCollectionViewCell.swift
//  Grocery
//
//  Created by Selladurai Murugesan on 4/5/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import UIKit

extension XIB {
    static let TopPickCollectionViewCell = "TopPickCollectionViewCell"
    static let OfferHeaderCollectionViewCell = "OfferHeaderCollectionViewCell"
    static let OfferCollectionViewCell = "OfferCollectionViewCell"
}

class TopPickCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var restaurantName: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cookTime: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var offer: UILabel!
    
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var timeImage: UIImageView!
    
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var incDecView: IncDecView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.dishImage?.layer.cornerRadius = (((UIScreen.main.bounds.width - 70)/2.6) * 0.6)/2
//        self.offerView.backgroundColor = AppColors.Red
    }
    
    func showAnimation() {
//        [offerView].forEach{$0?.isHidden = true}
//        bgView.backgroundColor = .white
        [dishImage, restaurantName, timeImage, cookTime].forEach{$0?.showAnimatedGradientSkeleton()}
    }
    
    func hideAnimation() {
//        [offerView].forEach{$0?.isHidden = false}
//        bgView.backgroundColor = [AppColors.Toppick1, AppColors.Toppick2][self.tag%2]
        [dishImage, restaurantName, timeImage, cookTime].forEach{$0?.hideSkeleton()}
    }

}

