//
//  TopItemCollectionViewCell.swift
//  GroceryTemp
//
//  Created by Selladurai Murugesan on 12/8/20.
//

import UIKit

extension XIB {
    static let TopItemCollectionViewCell = "TopItemCollectionViewCell"
}

class TopItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var quantityLbl1: UILabel!
    @IBOutlet weak var cartActionView: CartActionView!
    @IBOutlet weak var quantityStack: UIStackView!
    @IBOutlet weak var unavailableBtn: UIButton!
    
    @IBOutlet weak var cartActionSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var spacingStack: UIStackView!
    
    @IBOutlet var skeletonViews: [UIView]!
    
    var didRequestToShowVariants: (()->Void)?
    
    var isForHotSeller = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isForHotSeller == true {
            self.productImage.cornerRadius = self.productImage.bounds.width/2
        }
    }
    
    @IBAction func showVariants() {
        self.didRequestToShowVariants?()
    }
    
    func setData(_ product: Product?) {
        
        self.productImage?.loadImage(product?.photo?.stringValue)
        self.productName?.text = product?.productName?.stringValue
        self.offerLbl?.text = product?.offerInfo
        self.offerLbl?.textColor = .black
        self.offerLbl?.superview?.isHidden = (product?.discount?.intValue ?? 0) <= 0
        self.offerLbl?.superview?.backgroundColor = .clear
        (self.offerLbl?.superview as? AppImageView)?.image
            = #imageLiteral(resourceName: "offerTag")
        (self.offerLbl?.superview as? AppImageView)?.tintColor
            = product?.offerColor
        self.priceLbl?.font = UIFont.systemFont(ofSize: 12)
        self.priceLbl?.attributedText = product?.topItemPriceInfoNew
        self.quantityLbl?.text = product?.sizeName?.stringValue
        self.quantityLbl1?.text = product?.sizeName?.stringValue
        if product?.priceStatus?.intValue == 1 {
            self.quantityLbl?.superview?.isHidden = true
            self.quantityLbl1?.superview?.isHidden = false
            self.quantityLbl1?.superview?.isHidden = product?.sizeName?.stringValue == "none"
        } else {
            self.quantityLbl?.superview?.isHidden = false
            self.quantityLbl1?.superview?.isHidden = true
            self.quantityLbl?.superview?.isHidden = product?.sizeName?.stringValue == "none"
        }
        
        if product?.comboStatus?.boolValue == true {
            self.quantityLbl?.superview?.isHidden = false
            self.quantityLbl?.text = "Combo"
            self.quantityLbl1?.text = "Combo"
        }
        //product?.productName?.stringValue.contains("ccoli") == true || product?.productName?.stringValue.contains("Small") == true || product?.productName?.stringValue.contains("Potato") == true
        
        
        
        self.cartActionView?.addBtn?.isHidden = (product?.quantity?.intValue ?? 0) > 0
        self.cartActionView?.incDecView.countLbl.text = product?.quantity?.stringValue
        self.cartActionView?.incDecView.superview?.isHidden = (product?.quantity?.intValue ?? 0) <= 0
        
        if product?.availability?.intValue == 1 {
            self.cartActionView?.isHidden = false
            self.quantityStack?.isHidden = false
            self.unavailableBtn?.isHidden = true
            
            if product?.sizeName?.stringValue == "none" && product?.comboStatus?.intValue == 0 {
                self.unavailableBtn?.isHidden = false
            }
            
        } else {
            self.cartActionView?.isHidden = true
            self.quantityStack?.isHidden = true
            self.unavailableBtn?.isHidden = false
            
//            if product?.sizeName?.stringValue == "none" && product?.comboStatus?.intValue == 0 {
//                self.unavailableBtn?.isHidden = false
//            } else {
//                self.cartActionView?.isHidden = false
//                self.quantityStack?.isHidden = false
//                self.unavailableBtn?.isHidden = true
//            }
        }
    }
    
    func showAnimation() {
        self.skeletonViews?.forEach{
            $0.isSkeletonable = true
        }
        self.skeletonViews?.forEach{
            $0.showAnimatedGradientSkeleton()
        }
        [productImage, productName, offerLbl, priceLbl, quantityLbl, quantityLbl1, unavailableBtn].forEach{$0?.showAnimatedGradientSkeleton()}
    }
    
    func hideAnimation() {
        self.skeletonViews?.forEach{
            $0.isSkeletonable = true
        }
        self.skeletonViews?.forEach{
            $0.hideSkeleton()
        }
        [productImage, productName, offerLbl, priceLbl, quantityLbl, quantityLbl1, unavailableBtn].forEach{$0?.hideSkeleton()}
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        if UIScreen.main.bounds.width <= 320 {
            if let constraint = self.cartActionSpaceConstraint {
                self.cartActionSpaceConstraint?.constant = 3
            }
            spacingStack?.spacing = 3
        }
        self.quantityLbl1?.adjustsFontSizeToFitWidth = true
        self.productName?.numberOfLines = 1
        self.productName?.lineBreakMode = .byWordWrapping
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        offerLbl?.text = nil
        productName?.text = nil
        productImage?.image = nil
        priceLbl?.text = nil
        quantityLbl?.text = nil
        quantityLbl1?.text = nil
        cartActionView?.isHidden = true
        quantityStack?.isHidden = true
        unavailableBtn?.isHidden = true
        
        isForHotSeller = false
        
        if UIScreen.main.bounds.width <= 320 {
            if let constraint = self.cartActionSpaceConstraint {
                self.cartActionSpaceConstraint?.constant = 3
            }
            spacingStack?.spacing = 3
        }
    }

}

extension String {
    var discount: String {
        return (self + " %  ").uppercased()
    }
    
    var off: String {
        return (self + " off  ").uppercased()
    }
    
    var priceStiked: NSMutableAttributedString {
        return NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.strikethroughColor : UIColor.lightGray, NSAttributedString.Key.strikethroughStyle: 2])
    }
    
    var priceStikedBlack: NSMutableAttributedString {
        return NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor : UIColor.black, NSAttributedString.Key.strikethroughColor : UIColor.darkGray, NSAttributedString.Key.strikethroughStyle: 1])
    }
    
    var priceStikedWhite: NSMutableAttributedString {
        return NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.strikethroughColor : UIColor.white, NSAttributedString.Key.strikethroughStyle: 2])
    }
    
    var attributted: NSAttributedString {
        return NSAttributedString(string: self)
    }
    
    var attributtedPriceSymbol: NSMutableAttributedString {
        return NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor : UIColor(hex: 0xDD6A26)])
    }
    
    var attributtedPriceSymbolGreen: NSMutableAttributedString {
        return NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor : UIColor(hex: 0x43D6B6)])
    }
    
    var attributtedBlack: NSMutableAttributedString {
        return NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
    }
    
    var newlineAttributted: NSAttributedString {
        return NSAttributedString(string: "\n"+self)
    }
    
    var spaceAttributted: NSMutableAttributedString {
        return NSMutableAttributedString(string: " "+self+" ")
    }
}

extension Product {
    
    var itemsInCombo: String? {
        return "Items in Combo - " + ((self.combos?.count ?? 0).stringValue ?? "")
    }
    
    var offerInfo: String? {
        return (self.discount?.intValue ?? 0) > 0 ? discountType?.stringValue.lowercased() == "flat" ? self.discount?.stringValue.price.off : self.discount?.stringValue.discount : " "
    }
    
    var priceInfo: NSAttributedString? {
        return {
            let str =  self.orignalPrice?.stringValue.price.priceStiked
            str?.append(self.price?.stringValue.price.newlineAttributted ?? NSAttributedString())
            return (self.discount?.intValue ?? 0) > 0 ? str : self.price?.stringValue.price.attributted
        }()
    }
    
    var topItemPriceInfo: NSAttributedString? {
        return {
            let str =  self.orignalPrice?.stringValue.price.priceStikedWhite
            str?.append(self.price?.stringValue.price.spaceAttributted ?? NSAttributedString())
            return (self.discount?.intValue ?? 0) > 0 ? str : self.price?.stringValue.price.attributted
        }()
    }
    
    var topItemPriceInfoNew: NSAttributedString? {
        return {
            let str1 = self.price?.stringValue.price.spaceAttributted
            let str =  (self.orignalPrice?.doubleValue ?? 0) > 0 ? self.orignalPrice?.stringValue.price.priceStiked : NSMutableAttributedString()
            str1?.append(str ?? NSAttributedString())
            return (self.discount?.intValue ?? 0) > 0 ? str1 : self.price?.stringValue.price.attributted
        }()
    }
    
    var productDetailPriceInfo: NSAttributedString? {
        return {
            let str1 = self.price?.stringValue.priceSpaceAttributed
            let str =  (self.orignalPrice?.doubleValue ?? 0) > 0 ? self.orignalPrice?.stringValue.price.priceStikedBlack : NSMutableAttributedString()
            str?.append(str1 ?? NSAttributedString())
            return (self.discount?.intValue ?? 0) > 0 ? str : self.price?.stringValue.priceAttributed
        }()
    }
}

extension ComboProduct {
    var productDetailPriceInfo: NSAttributedString? {
        return {
            let str1 = self.price?.stringValue.price.spaceAttributted
            let str =  (self.original_price?.doubleValue ?? 0) > 0 ? self.original_price?.stringValue.price.priceStiked : NSMutableAttributedString()
            str1?.append(str ?? NSAttributedString())
            return str1
        }()
    }
}
