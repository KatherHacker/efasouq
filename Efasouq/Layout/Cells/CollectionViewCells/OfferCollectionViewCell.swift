//
//  OfferCollectionViewCell.swift
//  Grocery
//
//  Created by Selladurai Murugesan on 4/5/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import UIKit

class OfferCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var cardInfoLbl: UILabel!
    
    @IBOutlet weak var titleView: UIView!
    
    @IBOutlet weak var verticalTextView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var bgImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleView?.rotate(degrees: 90, clockwise: false)
        // Initialization code
    }
    
    func showAnimation() {
        [verticalTextView, infoView].forEach{$0?.isHidden = true}
        bgImage?.showAnimatedGradientSkeleton()
    }
    
    func hideAnimation() {
       [verticalTextView, infoView].forEach{$0?.isHidden = false}
        bgImage?.hideSkeleton()
    }

}


class OfferTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var cardInfoLbl: UILabel!
    
    @IBOutlet weak var titleView: UIView!
    
    @IBOutlet weak var verticalTextView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var bgImageWidth: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleView?.rotate(degrees: 90, clockwise: false)
        // Initialization code
    }
    
    func showAnimation() {
        [verticalTextView, infoView].forEach{$0?.isHidden = true}
        bgImage?.showAnimatedGradientSkeleton()
    }
    
    func hideAnimation() {
       [verticalTextView, infoView].forEach{$0?.isHidden = false}
        bgImage?.hideSkeleton()
    }

}

extension StoryBoradID {
    static let OfferTableViewCell = "OfferTableViewCell"
}
