//
//  OfferHeaderCollectionViewCell.swift
//  Grocery
//
//  Created by Selladurai Murugesan on 4/5/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import UIKit

class OfferHeaderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
