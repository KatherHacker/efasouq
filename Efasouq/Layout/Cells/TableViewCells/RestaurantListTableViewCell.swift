//
//  RestaurantListTableViewCell.swift
//  Grocery
//
//  Created by Selladurai Murugesan on 3/26/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import UIKit

class RestaurantListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var dishName: UILabel!
    @IBOutlet weak var dishCountryType: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var cookTime: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var divider: UIView!
    @IBOutlet weak var timeImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func showAnimation() {
        [self.ratingView, self.divider].forEach{$0?.isHidden = true}
        [self.dishImage, self.timeImage, self.dishName, self.dishCountryType, self.cookTime, self.price].forEach{$0.showAnimatedGradientSkeleton() }
        
    }
    
    func hideAnimation() {
        [self.ratingView, self.divider].forEach{$0?.isHidden = false}
        [self.dishImage, self.timeImage, self.dishName, self.dishCountryType, self.cookTime, self.price].forEach{$0?.hideSkeleton() }
    }
    
}

class HotSellerTableViewCell: RestaurantListTableViewCell {
}
