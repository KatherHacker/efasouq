//
//  DropDownTableViewCell.swift
//  Grocery
//
//  Created by Selladurai Murugesan on 4/12/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import UIKit
import DropDown

class DropDownTableViewCell: DropDownCell {
    
    @IBOutlet weak var countLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        countLbl?.font = UIFont.systemFont(ofSize: 13)
        optionLabel?.font = UIFont.systemFont(ofSize: 13)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
