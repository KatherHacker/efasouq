//
//  GradientView.swift
//  Complexionz
//
//  Created by Appcoup on 9/5/19.
//  Copyright © 2019 Shamlatech. All rights reserved.
//
import UIKit
import Foundation

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColor:   UIColor = AppColors.Tapestry { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = AppColors.Tapestry { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    @IBInspectable var isReverseTheme:    Bool =  false { didSet { updatePoints() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = isReverseTheme ? [endColor.cgColor, startColor.cgColor] : [startColor.cgColor, endColor.cgColor]
    }
    
    var gradient: CAGradientLayer {
        return self.gradientLayer
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updatePoints()
        updateLocations()
        updateColors()
    }
}

class AppGradientView: GradientView {
    
    override func updateLocations() {
        gradientLayer.locations = [0.0/*, 0.25, 0.50, 0.75*/, 1.0]
    }
    
    override func updatePoints() {
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)//CGPoint(x: 0, y: 0.5)//CGPoint(x: 1, y: 0)
        gradientLayer.endPoint   = CGPoint(x: 0.5, y: 1)//CGPoint(x: 1, y: 0.5)//CGPoint(x: 0, y: 1)
    }
    
    override func updateColors() {
        gradientLayer.colors = [AppColors.AppLightGreen.withAlphaComponent(0.2).cgColor, /*AppColors.AppLightGreen.withAlphaComponent(0.1).cgColor, AppColors.AppLightGreen.withAlphaComponent(0.025).cgColor, AppColors.RedAlpha.withAlphaComponent(0.05).cgColor,*/ AppColors.RedAlpha.cgColor]
    }
    
}
