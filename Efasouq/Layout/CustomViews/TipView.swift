//
//  TipView.swift
//  Grocery
//
//  Created by Selladurai Murugesan on 3/24/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import UIKit

extension XIB {
    static let TipView = "TipView"
}

class TipView: AppImageView {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var tipBtn: UIButton!
    
    var didTapOnBtn: (()->Void)?
    
    @IBAction func tapOnBtn() {
        didTapOnBtn?()
    }

}
