//
//  CartCountView.swift
//  Grocery
//
//  Created by Selladurai Murugesan on 3/19/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import UIKit

extension XIB {
    static let CartCountView = "CartCountView"
}

class CartCountView: AppImageView {
    
    @IBOutlet weak var countLbl: UILabel!
    
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var addToCartView: UIView!
    
    var didRequestToAddCart: (()->Void)?
    var didRequestToIncreaseProductCount: (()->Void)?
    var didRequestToDecreaseProductCount: (()->Void)?
    
    @IBAction func addToCart() {
        didRequestToAddCart?()
    }

    @IBAction func increase() {
        didRequestToIncreaseProductCount?()
    }
    
    @IBAction func decrease() {
        didRequestToDecreaseProductCount?()
    }
    
}
