//
//  RestaurantMenuView.swift
//  Grocery
//
//  Created by Selladurai Murugesan on 3/21/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import UIKit

extension XIB {
    static let RestaurantMenuView = "RestaurantMenuView"
}

class RestaurantMenuView: AppImageView {
    
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var anchorView: UIView!
    
    var didRequestToShowMenu: (()->Void)?
    
    @IBAction func showMenu() {
        didRequestToShowMenu?()
    }

}
