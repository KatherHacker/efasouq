//
//  ProductCartBarView.swift
//  Grocery
//
//  Created by Selladurai Murugesan on 3/18/20.
//  Copyright © 2020 Selladurai Murugesan. All rights reserved.
//

import UIKit

struct XIB {
    static let ProductCartBarView = "ProductCartBarView"
}

@IBDesignable class ProductCartBarView: AppImageView {
    
    @IBOutlet weak var itemsCount: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var viewCartBtn: UIButton!
    
    var didRequestToViewCart: (()->Void)?
    
    @IBAction func viewCart() {
        didRequestToViewCart?()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
