//
//  PaddableTextField.swift
//  Taxi
//
//  Created by SELLADURAI on 11/02/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class PaddableTextField: AppTextField {
    
    var padding = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    
    @IBInspectable var leftP: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    @IBInspectable var rightP: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    @IBInspectable var topP: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    @IBInspectable var bottomP: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    func adjustPadding() {
        padding = UIEdgeInsets(top: topP, left: leftP, bottom: bottomP, right: rightP)
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: topP, left: leftP, bottom: bottomP, right: rightP))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: topP, left: leftP, bottom: bottomP, right: rightP))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: topP, left: leftP, bottom: bottomP, right: rightP))
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.rightViewRect(forBounds: bounds)
        return rect.inset(by: UIEdgeInsets(top: topP, left: leftP, bottom: bottomP, right: rightP))
    }
}
