//
//  SocialLoginManager.swift
//  Complexionz
//
//  Created by Appcoup on 8/20/19.
//  Copyright © 2019 Shamlatech. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import FirebaseAuth
import Firebase

let INSTAGRAM_GRAPHURL = "https://graph.instagram.com/"
let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
let INSTAGRAM_CLIENT_ID  = "2531425683770192"
let INSTAGRAM_CLIENTSERCRET = "eb3a6bced023d671cef70d72611f5760"
let INSTAGRAM_REDIRECT_URI = "https://grocery.co.uk/"//"http://localhost:3000/users/auth/instagram/callback"
let INSTAGRAM_SCOPE = "basic"//"public_content+likes+comments+relationships"

struct Constants {
    struct INSTAGRAM_IDS {
        static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
        static let  INSTAGRAM_LONG_TOKEN = "https://graph.instagram.com/access_token"
        static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
        static let INSTAGRAM_CLIENT_ID  = "2531425683770192"
        static let INSTAGRAM_CLIENTSERCRET = "eb3a6bced023d671cef70d72611f5760"
        static let INSTAGRAM_REDIRECT_URI = "https://complexionz.co.uk/"// "https://swisscuipdo.com/"
        static let INSTAGRAM_ACCESS_TOKEN =  "6271106988.1677ed0.f4e3360eb4c048c1840bb258b6496852"
        static let INSTAGRAM_SCOPE = "basic"//"user_profile,user_media"//"basic"//"public_content+likes+comments+relationships"
    }
}

class SocialLogin: NSObject {
    static let shared = SocialLogin()
    private override init(){ }
    var successBlock: ((String?, String?, [String: Any]?) -> ())?
    var failureBlock:((Error?) -> ())?
    var facebookReadPermissions = ["public_profile", "email"/*, "user_friends"*/]
    
    func loginToFacebook(_ controller: UIViewController!, withSuccess successBlock: @escaping (_ token: String?, _ userId: String?, _ userInfo: [String: AnyObject]?) -> (), andFailure failureBlock: @escaping (Error?) -> ()) {
        LoginManager().logOut()
        if AccessToken.current != nil {
            return
        }
        
        LoginManager().logIn(permissions: self.facebookReadPermissions, from: controller) { (result, error) in
            if error != nil {
                LoginManager().logOut()
                failureBlock(error)
            } else if result?.isCancelled == true {
                LoginManager().logOut()
                failureBlock(nil)
            } else {
                var allPermsGranted = true
                let grantedPermissions = result?.grantedPermissions.map( {"\($0)"} )
                for permission in self.facebookReadPermissions {
                    if grantedPermissions?.contains(permission) == false {
                        allPermsGranted = false
                        break
                    }
                }
                if allPermsGranted {
                    // Do work
                    let fbToken = result?.token?.tokenString
                    let fbUserID = result?.token?.userID
                    if(AccessToken.current != nil){
                        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                            var dict = [String: AnyObject]()
                            if (error == nil){
                                dict = result as! [String : AnyObject]
                                successBlock(fbToken,fbUserID,dict)
                            } else {
                                successBlock(fbToken,fbUserID,nil)
                            }
                        })
                    } else {
                        successBlock(fbToken,fbUserID,nil)
                    }
                } else {
                    failureBlock(nil)
                }
            }
        }
    }
    
    func loginToGoogle(_ controller: UIViewController!, withSuccess successBlock: @escaping (_ token: String?, _ userId: String?, _ userInfo: [String: Any]?) -> (), andFailure failureBlock: @escaping (Error?) -> ()) {
        GIDSignIn.sharedInstance()?.signOut()
        self.successBlock = successBlock
        self.failureBlock = failureBlock
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = controller
        GIDSignIn.sharedInstance().signIn()
    }
    
    fileprivate func googleSignOut() {
        GIDSignIn.sharedInstance().signOut()
    }
    
    deinit{
        self.successBlock = nil
        self.failureBlock = nil
    }
}

extension UIViewController {
    
}

extension SocialLogin: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if let error = error {
            self.failureBlock?(error)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let err = error {
                self.failureBlock?(err)
//                self.successBlock?(authentication.accessToken,authResult?.user.email,nil)
                return
            }
            if let userInfo = authResult?.additionalUserInfo?.profile {
                self.successBlock?(authentication.accessToken,authResult?.user.email,userInfo)
            }
            self.googleSignOut()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            self.failureBlock?(error)
            googleSignOut()
        }
    }
}

extension User: JSONSerializablee {
    
}

class MainAppDelegate: UIResponder, UIApplicationDelegate {
    
    var enableGoogleSignIn = false
    var enableFacebookSignIn = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        if enableFacebookSignIn {
            ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
            LoginManager().reauthorizeDataAccess(from: UIApplication.shared.keyWindow?.rootViewController ?? UIViewController()) { (result, error) in
            }
        }
        if enableGoogleSignIn {
//            FirebaseApp.configure()
            GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        }
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if enableGoogleSignIn {
            
            return GIDSignIn.sharedInstance()?.handle(url) ?? false//GIDSignIn.sharedInstance().handle(url,
//                                                     sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
//                                                     annotation: [:])
        }
        return false
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if enableGoogleSignIn {
            return GIDSignIn.sharedInstance()?.handle(url) ?? false//GIDSignIn.sharedInstance().handle(url,
//                                                     sourceApplication: sourceApplication,
//                                                     annotation: annotation)
        }
        if enableFacebookSignIn {
            let isFacebookURL = url.scheme != nil && url.scheme!.hasPrefix("fb\(Settings.appID ?? "")") && url.host == "authorize"
            if isFacebookURL {
                return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
            }
        }
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if enableFacebookSignIn {
            AppEvents.activateApp()
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}

//Usage

/*

@IBAction func googleBtnAction(_ sender: Any) {
    SocialLogin.shared.loginToGoogle(self, withSuccess: { (accessToken, userId, userInfo) in
        let credential = Credential.init(email: userId ?? "", source: "google", password: "")
        credential.token = accessToken
        credential.userInfo = userInfo
        self.delegate?.loginViewController(self, didRequestToAuthenticateCredential: credential)
    }) { (error) in
        if let err = error {
            self.showError(err)
        }
    }
}
@IBAction func faceBookBtnAction(_ sender: Any) {
    SocialLogin.shared.loginToFacebook(self, withSuccess: { (accessToken, userId, userInfo) in
        let credential = Credential.init(email: userId ?? "", source: "facebook", password: "")
        credential.token = accessToken
        credential.userInfo = userInfo
        self.delegate?.loginViewController(self, didRequestToAuthenticateCredential: credential)
        //            self.delegate?.loginViewController(self, didRequestToAuthenticateCredential: credential)
    }) { (error) in
        if let err = error {
            self.showError(err)
        }
    }
}

 */


public protocol JSONRepresentable {
    var keys: [String: String] { get }
    var innerKeys: [String: String] { get }
    var outterKeys: [String] { get }
    var JSONRepresentation: [String: Any] { get }
    var json: String? { get }
}

public protocol JSONSerializablee: JSONRepresentable {
}

public extension JSONSerializablee {
    var JSONRepresentation: [String: Any] {
        var representation = [String: Any]()
        let mapKeys = self.keys
        
        func parseValue(_ value: Any) -> Any {
            switch value {
            case let value as Date :
                return value.getUTCFormateDate()
            default:
                return "\(value)"
            }
        }
        
        for case let (label?, value) in Mirror(reflecting: self).children {
            let key = label.mapLabelWithKeys(mapKeys)
            if innerKeys.keys.contains(key) {
                var innerRepresentation = [String: Any]()
                innerRepresentation[label] = parseValue(value)
                if let innerKey = innerKeys[label] {
                    representation[innerKey] = innerRepresentation as AnyObject?
                }
            } else if outterKeys.contains(key) {
                if let val = value as? JSONSerializablee {
                    for case let (label?, value) in Mirror(reflecting: val).children {
                        let key = label.mapLabelWithKeys(val.keys)
                        representation[key] = parseValue(value)
                    }
                } else if value is NSObject {
                    representation[key] = parseValue(value)
                }
            } else {
                switch value {
                case let value as JSONRepresentable:
                    representation[key] = value.JSONRepresentation
                case let value as [JSONRepresentable]:
                    var arrayValues = [Any]()
                    for arrayValue in value {
                        arrayValues.append(arrayValue.JSONRepresentation as Any)
                    }
                    representation[key] = arrayValues
                case let value as [String: JSONRepresentable]:
                    var dictionary = [String: Any]()
                    for (key, value) in value {
                        dictionary[key.mapLabelWithKeys(mapKeys)] = value.JSONRepresentation as Any
                    }
                    representation[key] = dictionary
                case let value as NSObject:
                    representation[key] = parseValue(value)
                default:
                    break
                }
            }
        }
        return representation
    }
    
    var keys: [String: String] {
        return [String: String]()
    }
    
    var innerKeys: [String: String] {
        return [String: String]()
    }
    
    var outterKeys: [String] {
        return [String]()
    }
    
    var json: String? {
        let representation = JSONRepresentation
        guard JSONSerialization.isValidJSONObject(representation) else {
            return nil
        }
        do {
            let data = try JSONSerialization.data(withJSONObject: representation, options: [.prettyPrinted])
            return String(data: data, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}

extension String {
    func mapLabelWithKeys(_ keys: [String: String]) -> String {
        if let key = keys[self] {
            return key
        } else {
            return self
        }
    }
}

let deviceId = UIDevice.current.identifierForVendor?.uuidString
