//
//  Router.swift
//  Complexionz
//
//  Created by Appcoup on 8/19/19.
//  Copyright © 2019 Shamlatech. All rights reserved.
//

import Foundation
import UIKit

class Router {
    
    static let main = UIStoryboard(name: "Main", bundle: Bundle.main)
    static let auth = UIStoryboard(name: "Auth", bundle: Bundle.main)
    static let home = UIStoryboard(name: "Home", bundle: Bundle.main)
    static let temp = UIStoryboard(name: "Temp", bundle: Bundle.main)
    
    static func setLoginViewControllerAsRoot() {
        UIApplication.shared.keyWindow?.rootViewController = Router.auth.instantiateViewController(withIdentifier: StoryBoradID.LoginNavigationController)
    }
    
    static func setDashboardViewControllerAsRoot() {

        UIApplication.shared.keyWindow?.rootViewController = Router.home.instantiateInitialViewController()
    }
    
}

struct StoryBoradID {
    struct Cell {
        
    }
    struct Segue {
        
    }
}
