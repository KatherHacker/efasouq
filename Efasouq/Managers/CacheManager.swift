//
//  CacheManager.swift
//  Tulyps iOS
//
//  Created by Appcoup on 03/09/15.
//  Copyright © 2015 Tulyps Pvt Ltd. All rights reserved.
//

import UIKit
import Foundation

/// Constant vaiable that contains name of the CacheManager
let cacheManagerName = "cachemanager"

/**
 This Enum is used to define what kind of info stored in the Cache
 
 - AuthToken:     User's Authentication Token String
 - UserFirstName: User's FirstName
 - UserLastName:  User's LastName
 - UserId:        User's Profile Id
 */
public enum CacheKey: String {
    case AuthToken = "AuthToken", RefreshToken = "RefreshToken", FCMToken = "FCMToken", DeviceId = "DeviceId", CustomerInfo = "CustomerInfo", UserId = "UserId"
}

/// Class for Cache
open class Cache: NSObject, NSCoding {
    override init() {
        super.init()
    }
    /// Private Dictinary variable to store the User's info
    fileprivate var cacheDic: [String: Any] = [:]
    fileprivate var applicationCacheDic: [String: Any] = [:]
    
    public required init(coder aDecoder: NSCoder) {
        if let dico: [String: Any] = aDecoder.decodeObject(forKey: "cacheDic") as? [String: Any] {
            self.cacheDic = dico
        }
        
        if let dico: [String: Any] = aDecoder.decodeObject(forKey: "applicationCacheDic") as? [String: Any] {
            self.applicationCacheDic = dico
        }
        
        super.init()
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.cacheDic, forKey: "cacheDic")
        aCoder.encode(self.applicationCacheDic, forKey: "applicationCacheDic")
    }
}

/// Class for Managing the application Cache
open class CacheManager {
    /// Varialbe for Cache instance
    var currentCache: Cache?
    /// Class variable for CacheManager's sharedInstance
    
    struct Static {
        static var instance: CacheManager?
        static var token: Int = 0
    }
    
    private static var excuteOnce: () = {
        Static.instance = CacheManager()
        // we try to load an existing CacheManager, otherwise we create a new one
        if let filepath = CacheManager.pathInDocDirectory(cacheManagerName) {
            if let mgr = NSKeyedUnarchiver.unarchiveObject(withFile: filepath) as? Cache {
                Static.instance!.currentCache = mgr
            }
        }
        if Static.instance!.currentCache == nil {
            Static.instance!.currentCache = Cache()
        }
    }()
    open class var sharedInstance: CacheManager {
        _ = CacheManager.excuteOnce
        return Static.instance!
    }
    
    /**
     Class Method to find the file location path of the specified file
     
     - parameter filename: fileName to find the location path
     
     - returns: location path of the given file
     */
    class func pathInDocDirectory(_ filename: String) -> String? {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        if paths.count > 0 {
            let path: String = paths[0]
            return path + "/" + filename
        }
        return nil
    }
    
    /**
     This Method is used to save the changes made in the cache
     */
    open func saveCache() {
        if let path = CacheManager.pathInDocDirectory(cacheManagerName) {
            NSKeyedArchiver.archiveRootObject(self.currentCache!, toFile: path)
        }
    }
    
    /**
     This Method is used to clear the cache
     */
    open func clearCache() {
        if let path = CacheManager.pathInDocDirectory(cacheManagerName) {
            self.currentCache = Cache()
            NSKeyedArchiver.archiveRootObject(self.currentCache!, toFile: path)
        }
    }
    
    /**
     This Method is used to clear the cache
     */
    open func clearUserData() {
        self.currentCache?.cacheDic = [:]
        self.saveCache()
    }
    /**
     This Method is used to get the value for the particular user info stored in the cache
     
     - parameter key: key string to get the value
     
     - returns: returns the object for the key mapped in cache
     */
    open func getObjectForKey(_ key: CacheKey) -> Any? {
        if let cacheValue = self.currentCache?.cacheDic[key.rawValue] {
            return cacheValue
        }
        return nil
    }
    
    /**
     This Method is used to set the object and key with in the cache
     
     - parameter value: value that is needed to store
     - parameter key:   key for the value
     */
    open func setObject(_ value: Any, key: CacheKey) {
        self.currentCache?.cacheDic[key.rawValue] = value
        CacheManager.sharedInstance.saveCache()
    }
    
    open func removeObjectForKey(_ key: CacheKey) {
        self.currentCache?.cacheDic.removeValue(forKey: key.rawValue)
    }
    
    /**
     This Method is used to get the value for the particular user info stored in the application cache
     
     - parameter key: key string to get the value
     
     - returns: returns the object for the key mapped in cache
     */
    open func getObjectInApplicationForKey(_ key: CacheKey) -> Any? {
        if let cacheValue = self.currentCache?.applicationCacheDic[key.rawValue] {
            return cacheValue
        }
        return nil
    }
    
    /**
     This Method is used to set the object and key with in the application cache
     
     - parameter value: value that is needed to store
     - parameter key:   key for the value
     */
    open func setObjectInApplication(_ value: Any, key: CacheKey) {
        self.currentCache?.applicationCacheDic[key.rawValue] = value
        CacheManager.sharedInstance.saveCache()
    }
    
    open func removeObjectInApplicationForKey(_ key: CacheKey) {
        self.currentCache?.applicationCacheDic.removeValue(forKey: key.rawValue)
    }
}
