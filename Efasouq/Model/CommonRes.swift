//
//  CommonRes.swift
//  //   DNDev
//
//  Created by Admin on 4/27/21.
//

import Foundation

struct CommonRes: JSONSerializable {
    let status: StatusInfo?
    let message, detail: CustomType?
    var isSuccess: Bool { return self.status?.isSuccess == true }
}

struct StatusInfo: JSONSerializable {
    let success, message: CustomType?
    var isSuccess: Bool { return self.success?.boolValue == true }
}

class GeneralResponseSingle<T: Decodable>: Decodable {
    var status: StatusInfo?
    var message, detail: String?
    var data : T?
    var isLoading: Bool? = false

    init() {
        self.data = nil
        self.message = nil
        self.detail = nil
        self.status = nil
        self.isLoading = false
    }
}

class GeneralResponseArray<T: Decodable>: Decodable {
    var status: StatusInfo?
    var message, detail: String?
    var data : [T]?
    var isLoading: Bool? = false
    var page, pages: CustomType?

    init() {
        self.data = []
        self.message = nil
        self.detail = nil
        self.status = nil
        self.isLoading = false
        self.page = nil
        self.pages = nil
    }
}
