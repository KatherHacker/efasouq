//
//  Model.swift
//  DNDev
//
//  Created by Admin on 4/27/21.
//

import Foundation
import UIKit

struct LoginReq: JSONSerializable {
    var username, password, user_role: String!
}

struct LoginRes: JSONSerializable {
    let status: StatusInfo?
    let token: CustomType?
    let data: UserInfo?
}

class UserInfo: NSObject, JSONSerializable, NSCoding {
    let id: CustomType?
    let name: CustomType?
    let username: CustomType?
    let email: CustomType?
    let isAdmin, token: CustomType?
    let refresh, access, userCategory: CustomType?

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case username
        case email
        case isAdmin
        case token
        case refresh, access, userCategory
    }

    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? CustomType
        name = aDecoder.decodeObject(forKey: "name") as? CustomType
        username = aDecoder.decodeObject(forKey: "username") as? CustomType
        email = aDecoder.decodeObject(forKey: "email") as? CustomType
        isAdmin = aDecoder.decodeObject(forKey: "isAdmin") as? CustomType
        token = aDecoder.decodeObject(forKey: "token") as? CustomType
        refresh = aDecoder.decodeObject(forKey: "refresh") as? CustomType
        access = aDecoder.decodeObject(forKey: "access") as? CustomType
        userCategory = aDecoder.decodeObject(forKey: "userCategory") as? CustomType
    }
}

class ProfileData: NSObject, JSONSerializable, NSCoding {
    var username: CustomType? // "krtesting67@gmail.com",
    var first_name: CustomType? // "Kamaleshwar",
    var last_name: CustomType? // "Rishis",
    var occupation: CustomType? // "Fashion Occupation",
    var city: CustomType? // "Coimbatore",
    var dob: CustomType? // "1994-05-19",
    var estb: CustomType? // "1994-05-20",
    var bio: CustomType? // "I am a fashion designer based out of coimbatore .",
    var products: CustomType? // "{\"0\": \"Accessory\", \"1\": \"Bag\", \"2\": \"Clothing\", \"3\": \"Footwear\", \"4\": \"False\", \"5\": \"About Us \"}",
    var occassion: CustomType? // "{\"0\": \"Casual\", \"1\": \"Ethinic\", \"2\": \"False\", \"3\": \"False\", \"4\": \"False\"}",
    var specialization: CustomType? // "{\"0\": \"Bridge\", \"1\": \"False\", \"2\": \"False\"}",
    var businessname: CustomType? // "My new boi business2"
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        username = aDecoder.decodeObject(forKey: "username") as? CustomType
        first_name = aDecoder.decodeObject(forKey: "first_name") as? CustomType
        last_name = aDecoder.decodeObject(forKey: "last_name") as? CustomType
        occupation = aDecoder.decodeObject(forKey: "occupation") as? CustomType
        city = aDecoder.decodeObject(forKey: "city") as? CustomType
        dob = aDecoder.decodeObject(forKey: "dob") as? CustomType
        estb = aDecoder.decodeObject(forKey: "estb") as? CustomType
        bio = aDecoder.decodeObject(forKey: "bio") as? CustomType
        products = aDecoder.decodeObject(forKey: "products") as? CustomType
        occassion = aDecoder.decodeObject(forKey: "occassion") as? CustomType
        specialization = aDecoder.decodeObject(forKey: "specialization") as? CustomType
        businessname = aDecoder.decodeObject(forKey: "businessname") as? CustomType
    }
}

class ProfileImage: NSObject, JSONSerializable, NSCoding {
    var image, bannerImage: CustomType?
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        image = aDecoder.decodeObject(forKey: "image") as? CustomType
        bannerImage = aDecoder.decodeObject(forKey: "bannerImage") as? CustomType
    }
}

struct ProfileReq {
    
    var image: UIImage!
    var bannerImage: UIImage!
    
    var bio: String!//Fashion Developer
    var dob: String!//1994-07-21
    var estb: String!//2016-08-23
    var location: String!//Coimbatore
    var occupation: String!//Fashion Designer by
    var products: String!//{"0":"False","1":"Bag","2":"Clothing","3":"Footwear","4":"False"}
    var occassion: String!//{"0":"False","1":"Ethnic","2":"False","3":"False","4":"False"}
    var specialization: String!//{"0":"Haute Couture","1":"False","2":"Ready To Wear/ Pret"}
    var first_name: String!//krtestinng
    var last_name: String!//Updatesdd
    
    var JSONRepresentation: [String: Any] {
        return [
            "bio": self.bio ?? "",
            "dob": self.dob ?? "",
            "estb": self.estb ?? "",
            "location": self.location ?? "",
            "occupation": self.occupation ?? "",
            "products": self.products ?? "",
            "occassion": self.occassion ?? "",
            "specialization": self.specialization ?? "",
            "first_name": self.first_name ?? "",
            "last_name": self.last_name ?? ""
        ]
    }
    
    var images: [String: UIImage] {
        return [
            "image": self.image ?? UIImage(),
            "bannerImage": self.bannerImage ?? UIImage()
        ]
    }
}

class ProfileInfo: NSObject, JSONSerializable, NSCoding {
    var userData: [ProfileData]?
    var userImage: ProfileImage?
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        userData = aDecoder.decodeObject(forKey: "userData") as? [ProfileData]
        userImage = aDecoder.decodeObject(forKey: "userImage") as? ProfileImage
    }
}


extension CacheManager {
    static var userInfo: UserInfo? {
        get {
            
            return CacheManager.sharedInstance.getObjectForKey(.UserInfo) as? UserInfo
        }
        set {
            if let val = newValue {
                CacheManager.sharedInstance.setObject(val, key: .UserInfo)
                CacheManager.sharedInstance.saveCache()
            }
        }
    }
}

