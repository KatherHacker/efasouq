//
//  Register.swift
//  DNDev
//
//  Created by Admin on 4/27/21.
//

import Foundation

struct RegisterReq: JSONSerializable {
    var name: String!
    var last_name: String!
    var first_name: String!
    var email: String!
    var password: String!
    var userCategory: String!
    var fcmToken: String!
}
