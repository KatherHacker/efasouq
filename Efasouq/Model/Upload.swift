//
//  UploadRes.swift
//  DNDev
//
//  Created by Admin on 4/27/21.
//

import Foundation

struct UploadInfo: JSONSerializable {
    var image_name, image_url: CustomType?
}
