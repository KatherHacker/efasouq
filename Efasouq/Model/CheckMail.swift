//
//  CheckMail.swift
//  DNDev
//
//  Created by Admin on 4/27/21.
//

import Foundation

struct CheckReq: JSONSerializable {
    var email: String!
}

struct CheckRes: JSONSerializable {
    var status: CheckInfo?
}

struct CheckInfo: JSONSerializable {
    var email_exist, message: CustomType?
    var isSuccess: Bool { return self.email_exist?.boolValue == true }
}
