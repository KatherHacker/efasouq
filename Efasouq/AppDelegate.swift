//
//  AppDelegate.swift
//  Efasouq
//
//  Created by Shamlatech's Mac book Air on 22/04/21.
//

import UIKit
import CoreData
import Reachability
import Firebase
import FirebaseMessaging

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private var reachability:Reachability!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        self.reachability = try! Reachability()
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkForReachability(notification:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        
        // Override point for customization after application launch.
        return true
    }
    
    @objc func checkForReachability(notification:NSNotification)
    {
        let networkReachability = notification.object as! Reachability;
        let remoteHostStatus = networkReachability.connection
        
        switch remoteHostStatus {
        case .wifi, .cellular:
            print("Reachable via WiFi")
            UserDefaults.standard.setValue("1", forKey: "CurrentNetworkStatus")
            UserDefaults.standard.synchronize()
            if self.keyWindow?.rootViewController?.vcToPresent == nil {
                self.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
            }
        case .unavailable:
            UserDefaults.standard.setValue("0", forKey: "CurrentNetworkStatus")
            UserDefaults.standard.synchronize()
            self.keyWindow?.rootViewController?.vcToPresent?.showAlertMessage("Please make sure you are connected to the internet.", title: "Network Connection Failed")
        case .none:
            break
        }
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "DNDev")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        NotificationCenter.default.post(name: NSNotification.Name.init("DID_RECEIVE_JOB_UPDATE"), object: nil, userInfo: notification.request.content.userInfo)
        completionHandler(
               [UNNotificationPresentationOptions.alert,
                UNNotificationPresentationOptions.sound,
                UNNotificationPresentationOptions.badge])
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        let cache = CacheManager.sharedInstance
        UserDefaults.fcmToken = fcmToken ?? ""
        cache.setObject(fcmToken as AnyObject, key: .FCMToken)
        cache.saveCache()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        NotificationCenter.default.post(name: NSNotification.Name.init("DID_RECEIVE_JOB_UPDATE"), object: nil, userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print(deviceToken)
        Messaging.messaging().apnsToken = deviceToken
        if let token = Messaging.messaging().fcmToken {
            let cache = CacheManager.sharedInstance
            UserDefaults.fcmToken = token
            cache.setObject(token as AnyObject, key: .FCMToken)
            cache.saveCache()
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
        self.connectToFCM()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response)
        NotificationCenter.default.post(name: NSNotification.Name.init("DID_RECEIVE_JOB_UPDATE"), object: nil, userInfo: response.notification.request.content.userInfo)
    }
    
    func connectToFCM() {
        Messaging.messaging().isAutoInitEnabled = true
        if let token = Messaging.messaging().fcmToken {
            let cache = CacheManager.sharedInstance
            cache.setObject(token as AnyObject, key: .FCMToken)
            UserDefaults.fcmToken = token
            cache.saveCache()
        }
    }
    
}

extension UserDefaults {
    static var fcmToken: String {
        get { return UserDefaults.standard.value(forKey: "FCMToken") as? String ?? "" }
        set { UserDefaults.standard.setValue(newValue, forKey: "FCMToken"); UserDefaults.standard.synchronize() }
    }
}

extension UIViewController {
    var vcToPresent: UIViewController? {
        return self.presentedViewController == nil ? self : nil
    }
}

