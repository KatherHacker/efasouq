//
//  JSONMapping.swift
//  //   DNDev
//
//  Created by Admin on 4/27/21.
//

import Foundation

protocol JSONSerializable : Codable {
    var JSONRepresentation : [String : Any] { get }
    var defaultCount: Int { get }
}

extension JSONSerializable {
    
    public var defaultCount: Int {
        return 6
    }
    
    var JSONRepresentation : [String : Any] {
        
        var representation = [String:Any]()
        
        for case let (label?, value) in Mirror(reflecting: self).children {
            
            switch value {
            case let value as Dictionary<String,Any>:
                
                if let val = value as? JSONSerializable {
                    representation[label] = val.JSONRepresentation as AnyObject
                } else {
                    representation[label] = value as AnyObject
                }
                
            case let value as Array<Any>:
                
                if let val = value as? [JSONSerializable]{
                    representation[label] = val.map({ $0.JSONRepresentation as AnyObject}) as AnyObject
                } else {
                    representation[label] = value as AnyObject
                }
                
            case let value as CustomType:
                if value.value is NSNull {
                    break
                } else {
                    representation[label] = value.value
                }
                    
                
            case let value as JSONSerializable:
                
                representation[label] = value.JSONRepresentation
                
            case let value as AnyObject :
                
                if value is NSNull {
                    break
                } else {
                    representation[label] = value
                }
                
                
//                representation[label] = value
                
            default: break
            }
        }
        return representation
    }
    
    // Convert to data by Encoding
    
    func toData()->Data? {
        
        do {
            
            return try JSONEncoder().encode(self)
            
        } catch let err {
            print("Error in Encoding ", self.JSONRepresentation, err.localizedDescription)
            return nil
        }
        
    }
    
    func transform<T>(from object : T.Type)->T? where T : Decodable {
        
        return self.JSONRepresentation.data?.getDecodedObject(from: object)
        
    }
    
}

extension Dictionary where Key == String, Value == Any {
    var data: Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
    }
}

class CustomType: NSObject, JSONSerializable, NSCoding {
    
    override init() {
        self.value = nil
    }
    
    init(value: AnyObject?) {
        self.value = value
    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        value = aDecoder.decodeObject(forKey: "value") as AnyObject
    }
    
    var value: AnyObject?
    
    var intValue: Int? {
        return Int(stringValue)
    }
    
    var boolValue: Bool {
        return stringValue == "1" || stringValue == "true"
    }
    
    var floatValue: Float? {
        return Float(stringValue)
    }
    
    var doubleValue: Double? {
        return Double(stringValue)
    }
    
    var stringValue: String {
        return "\(self.value ?? "" as AnyObject)"
    }
    
    var object: Dictionary<String, CustomType>? {
        return value as? Dictionary<String, CustomType>
    }
    
    var array: Array<CustomType>? {
        return value as? Array<CustomType>
    }
    
    required init(from decoder: Decoder) throws {
        let container = try? decoder.singleValueContainer()
        self.value = try? container?.decode(Int.self) as AnyObject
        if self.value == nil {
            self.value = try? container?.decode(Float.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(String.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Double.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Float32.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Float64.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Int8.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Int16.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Int32.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Int64.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt8.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt16.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt32.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt64.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Dictionary<String, CustomType>.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Array<CustomType>.self) as AnyObject
        }
    }
    
    func encode(to encoder: Encoder) throws {
        
    }
}

/*
struct JSONCodingKeys: CodingKey {
    var stringValue: String
    init?(stringValue: String) {
        self.stringValue = stringValue
    }
    var intValue: Int?
    init?(intValue: Int) {
        self.init(stringValue: "\(intValue)")
        self.intValue = intValue
    }
}

extension KeyedDecodingContainer {
    func decode(_ type: Dictionary<String, Any>.Type, forKey key: K) throws -> Dictionary<String, Any> {
        let container = try self.nestedContainer(keyedBy: JSONCodingKeys.self, forKey: key)
        return try container.decode(type)
    }
    func decodeIfPresent(_ type: Dictionary<String, Any>.Type, forKey key: K) throws -> Dictionary<String, Any>? {
        guard contains(key) else {
            return nil
        }
        return try decode(type, forKey: key)
    }
    func decode(_ type: Array<Any>.Type, forKey key: K) throws -> Array<Any> {
        var container = try self.nestedUnkeyedContainer(forKey: key)
        return try container.decode(type)
    }
    func decodeIfPresent(_ type: Array<Any>.Type, forKey key: K) throws -> Array<Any>? {
        guard contains(key) else {
            return nil
        }
        return try decode(type, forKey: key)
    }
    func decode(_ type: Dictionary<String, Any>.Type) throws -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        for key in allKeys {
            if let boolValue = try? decode(Bool.self, forKey: key) {
                dictionary[key.stringValue] = boolValue
            } else if let stringValue = try? decode(String.self, forKey: key) {
                dictionary[key.stringValue] = stringValue
            } else if let intValue = try? decode(Int.self, forKey: key) {
                dictionary[key.stringValue] = intValue
            } else if let doubleValue = try? decode(Double.self, forKey: key) {
                dictionary[key.stringValue] = doubleValue
            } else if let nestedDictionary = try? decode(Dictionary<String, Any>.self, forKey: key) {
                dictionary[key.stringValue] = nestedDictionary
            } else if let nestedArray = try? decode(Array<Any>.self, forKey: key) {
                dictionary[key.stringValue] = nestedArray
            }
        }
        return dictionary
    }
}
extension UnkeyedDecodingContainer {
    mutating func decode(_ type: Array<Any>.Type) throws -> Array<Any> {
        var array: [Any] = []
        while isAtEnd == false {
            if let value = try? decode(Bool.self) {
                array.append(value)
            } else if let value = try? decode(Double.self) {
                array.append(value)
            } else if let value = try? decode(String.self) {
                array.append(value)
            } else if let nestedDictionary = try? decode(Dictionary<String, Any>.self) {
                array.append(nestedDictionary)
            } else if let nestedArray = try? decode(Array<Any>.self) {
                array.append(nestedArray)
            }
        }
        return array
    }
    mutating func decode(_ type: Dictionary<String, Any>.Type) throws -> Dictionary<String, Any> {
        let nestedContainer = try self.nestedContainer(keyedBy: JSONCodingKeys.self)
        return try nestedContainer.decode(type)
    }
}
*/
