////
////  AppView.swift
////  TaxiPickup
////
////  Created by Gireesh on 02/01/21.
////  Copyright © 2021 PPD Solutions. All rights reserved.
////
//
//import UIKit
//
//@IBDesignable
//class AppView: UIView {
//
//    /*
//    // Only override draw() if you perform custom drawing.
//    // An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//        // Drawing code
//    }
//    */
//
//    @IBInspectable var backgroundThemeColor: Int = -1 {
//        didSet {
//            self.backgroundColor = ColorTheme.init(rawValue: self.backgroundThemeColor)?.color ?? UIColor.black
//        }
//    }
//    @IBInspectable var isBottomViewRound:Int = 0
//
//    override init(frame : CGRect) {
//        super.init(frame : frame)
//        setup()
//    }
//
//    convenience init() {
//        self.init(frame:CGRect.zero)
//        setup()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setup()
//    }
//
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        setup()
//    }
//
//    override func prepareForInterfaceBuilder() {
//        super.prepareForInterfaceBuilder()
//        setup()
//    }
//
//    func setup() {
//
//    }
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        if cornerRadius != 0{
//            roundCorners(corners:[.topLeft, .topRight], radius: cornerRadius)
//        }
//        if isBottomViewRound == 1{
//            roundCorners(corners: [.bottomLeft,.bottomRight], radius: self.frame.size.width/2)
//        }else if isBottomViewRound == 2{
//            self.layer.cornerRadius = self.frame.size.height/2
//        }
//    }
//
//
//    @IBInspectable
//    var isRounded: Bool {
//        get {
//            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.isRounded) as? Bool else {
//                return false
//            }
//            return value
//        }
//        set(newValue) {
//            objc_setAssociatedObject(self, &AssociatedKeys.isRounded, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            if newValue {
//                self.cornerRadius = bounds.width/2
//            }
//        }
//    }
//
//    @IBInspectable
//    var cornerRadius: CGFloat = 0
//
//    @IBInspectable
//    var borderWidth: CGFloat {
//        get {
//            return layer.borderWidth
//        }
//        set {
//            layer.borderWidth = newValue
//        }
//    }
//
//    @IBInspectable
//    var borderColor: UIColor? {
//        get {
//            if let color = layer.borderColor {
//                return UIColor(cgColor: color)
//            }
//            return nil
//        }
//        set {
//            if let color = newValue {
//                layer.borderColor = color.cgColor
//            } else {
//                layer.borderColor = nil
//            }
//        }
//    }
//
//    @IBInspectable
//    var shadowRadius: CGFloat {
//        get {
//            return layer.shadowRadius
//        }
//        set {
//            layer.shadowRadius = newValue
//        }
//    }
//
//    @IBInspectable
//    var shadowOpacity: Float {
//        get {
//            return layer.shadowOpacity
//        }
//        set {
//            layer.shadowOpacity = newValue
//        }
//    }
//
//    @IBInspectable
//    var shadowOffset: CGSize {
//        get {
//            return layer.shadowOffset
//        }
//        set {
//            layer.shadowOffset = newValue
//        }
//    }
//
//    @IBInspectable
//    var shadowColor: UIColor? {
//        get {
//            if let color = layer.shadowColor {
//                return UIColor(cgColor: color)
//            }
//            return nil
//        }
//        set {
//            if let color = newValue {
//                layer.shadowColor = color.cgColor
//            } else {
//                layer.shadowColor = nil
//            }
//        }
//    }
//}
