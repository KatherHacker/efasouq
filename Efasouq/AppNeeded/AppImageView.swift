//
//  AppImageView.swift
//  Taxi
//
//  Created by ShamlaTech on 8/4/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

@IBDesignable
class AppImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var imageThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var backgroundThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var borderTheme: Int = -1 {
        didSet {
            setup()
        }
    }
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        setup()
    }
    
    convenience init() {
        self.init(frame:CGRect.zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
//    override var image: UIImage? {
//        get {
//            return self.appImage
//        }
//        set {
//            self.appImage = newValue
//        }
//    }
//
//    @IBInspectable
//    var appImage: UIImage? {
//        didSet {
//            setup()
//        }
//    }
    
    func setup() {
        if self.imageThemeColor != -1 {
            self.tintColor = ColorTheme.init(rawValue: self.imageThemeColor)?.color ?? UIColor.blue
            self.image = self.image?.withRenderingMode(.alwaysTemplate)
        } else {
            self.image = self.image?.withRenderingMode(.alwaysOriginal)
        }
        
        if self.backgroundThemeColor != -1 {
            self.backgroundColor = (ColorTheme.init(rawValue: self.backgroundThemeColor)?.color ?? UIColor.clear)
        } else {

        }
        
        if self.borderTheme != -1 {
            self.layer.borderColor = (ColorTheme.init(rawValue: self.borderTheme)?.color ?? UIColor.clear).cgColor
        } else {
//            super.image = self.appImage
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    @IBInspectable
    override var isRounded: Bool {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.isRounded) as? Bool else {
                return false
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.isRounded, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            if newValue {
                self.cornerRadius = bounds.width/2
            }
        }
    }
    
    @IBInspectable
    override var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    override var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    override var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    override var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    override var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    override var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    override var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
