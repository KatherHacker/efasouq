//
//  AppTheme.swift
//  Taxi
//
//  Created by ShamlaTech on 7/31/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import Foundation
import UIKit

struct AppColors {
   // static let colorPrimary = UIColor.init(netHex: 0x590000)
   // static let colorPrimaryDark = UIColor.init(netHex: 0xdf0000)
   // static let colorAccent = UIColor.init(netHex: 0xFF0000)
    static let primaryButton = UIColor.init(netHex: 0xf9f9f9)
    static let primaryButtonClick = UIColor.init(netHex: 0xFFFFFF)
    static let primaryButtonTrans = UIColor.init(netHex: 0x173667)
    static let primaryButtonTransClick = UIColor.init(netHex: 0xd7004a)
    //static let gStartColor = UIColor.init(netHex: 0xed1b59)
    //static let gEndColor = UIColor.init(netHex: 0xed1e24)
    static let gStartColorClick = UIColor.init(netHex: 0xdc004c)
    static let gEndColorClick = UIColor.init(netHex: 0xdf0101)
    static let colorOtherMessageBack = UIColor.init(netHex: 0xFF0058)
    static let colorCCP = UIColor.init(netHex: 0xFFFFFF)
    
    //tamil
    static let colorPrimary = UIColor.init(netHex: 0xF5ECE7)
    static let colorPrimaryDark = UIColor.init(netHex: 0x173667)
    static let colorAccent = UIColor.init(netHex: 0xF5ECE7)
    static let gStartColor = UIColor.init(netHex: 0x173667)
    static let gEndColor = UIColor.init(netHex: 0x173667)
   // static let gEndColor = UIColor.init(netHex: 0xEFBF01)

    static let backgroundLightThemeColor = UIColor.init(netHex: 0xFEF8DD)
    static let colorAppBackground = UIColor.init(netHex: 0xFAF8F8)
    
    
    static let colorNormalText = UIColor.init(netHex: 0x173667)
    static let colorBlueButton = UIColor.init(netHex: 0xFFFFFF)
    
    static var colorTextDefault: UIColor { return UIColor.init(netHex: 0x0b0b0b) }
    static var colorTextLightDefault: UIColor { return UIColor.init(netHex: 0xa1a1a1) }
    static var colorButtonLightBackground: UIColor { return UIColor.lightGray.withAlphaComponent(0.2) }

     
    static var colorStatusSearching: UIColor { return UIColor.init(netHex: 0x1763A6) }
    static var ratingColor: UIColor { return UIColor.init(netHex: 0xFFB100) }
    static var colorStatusAccepted: UIColor { return UIColor.init(netHex: 0x3FD449) }
    static var colorStatusArrived: UIColor { return UIColor.init(netHex: 0xF25430) }
    static var colorStatusRunning: UIColor { return UIColor.init(netHex: 0x237529) }
    static var colorStatusEnd: UIColor { return UIColor.init(netHex: 0x114A7D) }
    static var colorStatusCompleted: UIColor { return UIColor.init(netHex: 0x194b1c) }
    static var colorStatusCancelled: UIColor { return UIColor.init(netHex: 0xF22600) }
    static var colorStatusNoDriver: UIColor { return UIColor.init(netHex: 0xF22657) }
    static var colorTextDate: UIColor { return UIColor.init(netHex: 0xa1a1a1) }
    
    static var colorWalletRechargeHeader: UIColor { return UIColor.init(netHex: 0x2ba80d01) }
    static var colorWalletRideHeader: UIColor { return UIColor.init(netHex: 0x2ba80d01) }
    static var colorWalletWithDrawHeader: UIColor { return UIColor.init(netHex: 0x2ba80d01) }
    
    static var colorWalletRecharge: UIColor { return UIColor.init(netHex: 0x0b9e00) }
    static var colorWalletRide: UIColor { return UIColor.init(netHex: 0xa80d01) }
    static var colorWalletWithDraw: UIColor { return UIColor.init(netHex: 0xa80d01) }
    
    static var colorButtonBackground: UIColor { return UIColor.lightGray }
    static var colorButtonBorder: UIColor { return UIColor.white }
    static var colorBlackButton: UIColor { return UIColor.black }
    static var colorBlackTextField: UIColor { return UIColor.black }
    static var colorPlaceHolderTextField: UIColor { return UIColor.lightGray }
    static var colorBackGround: UIColor { return UIColor.darkGray }

    //static var colorOffline: UIColor { return UIColor(netHex: 0x017302) }
    static var colorOnlinegreen: UIColor { return UIColor(netHex: 0x017302) }
    static var colorOffline: UIColor { return UIColor(netHex: 0x590000) }
    
    static var dimmedMailColor:UIColor { return  UIColor.init(netHex: 0xFF0000, alpha: 0.2)}
}

enum ColorTheme: Int {
    case
    primary = 1,
    primaryDark = 2,
    accent = 3,
    primaryButton = 4,
    primaryButtonClick = 5,//White
    primaryButtonTrans = 6,
    primaryButtonTransClick = 7,
    gStartColor = 8,
    gEndColor = 9,
    gStartColorClick = 10,
    gEndColorClick = 11,
    colorOtherMessageBack = 12,
    colorCCP = 13,
    colorNormalText = 14,
    colorBlueButton = 15,
    colorButtonBackground = 16,
    colorButtonBorder = 17,
    colorBlackButton = 18,//black
    colorBlackTextField = 19,
    colorTextDefault = 20,
    colorTextLightDefault = 21,
    colorButtonLightBackground = 22,
    colorPlaceHolderTextField = 23,
    colorOnline = 24,
    colorAppBackground = 25,
    backgroundLightThemeColor = 26,
    colorOnlinegreen = 27,
    colorBackGround = 28
    

    var color: UIColor {
        switch self {
        case .primary: return AppColors.colorPrimary
        case .primaryDark: return AppColors.colorPrimaryDark
        case .accent : return AppColors.colorAccent
        case .primaryButton : return AppColors.primaryButton
        case .primaryButtonClick : return AppColors.primaryButtonClick
        case .primaryButtonTrans : return AppColors.primaryButtonTrans
        case .primaryButtonTransClick : return AppColors.primaryButtonTransClick
        case .gStartColor : return AppColors.gStartColor
        case .gEndColor : return AppColors.gEndColor
        case .gStartColorClick : return AppColors.gStartColorClick
        case .gEndColorClick : return AppColors.gEndColorClick
        case .colorOtherMessageBack : return AppColors.colorOtherMessageBack
        case .colorCCP : return AppColors.colorCCP
        case .colorBlueButton : return AppColors.colorBlueButton
        case .colorNormalText: return AppColors.colorNormalText
        case .colorButtonBackground: return AppColors.colorButtonBackground
        case .colorButtonBorder: return AppColors.gStartColor//AppColors.colorButtonBorder
        case .colorBlackButton: return AppColors.colorBlackButton
        case .colorBlackTextField: return AppColors.colorBlackTextField
        case .colorTextDefault: return AppColors.colorTextDefault
        case .colorTextLightDefault: return AppColors.colorTextLightDefault
        case .colorButtonLightBackground: return AppColors.colorButtonLightBackground
        case .colorPlaceHolderTextField: return AppColors.colorPlaceHolderTextField
        //case .colorOnline: return AppColors.colorOffline
        case .colorAppBackground: return AppColors.colorAppBackground
        case .backgroundLightThemeColor: return AppColors.backgroundLightThemeColor
        case .colorOnlinegreen: return AppColors.colorOnlinegreen
        case .colorOnline: return AppColors.colorOffline
        case .colorBackGround: return AppColors.colorBackGround

        }
    }
}

struct Theme {
    var background: UIColor
    var text: UIColor
    var tint: UIColor
    
    init(background: UIColor = UIColor.clear, text: UIColor = AppColors.colorPrimaryDark, tint: UIColor = UIColor.blue) {
        self.background = background
        self.text = text
        self.tint = tint
    }
}

//
//  AppTheme.swift
//   DNDev
//
//  Created by Admin on 7/31/19.
//  Copyright © 2021 Admin. All rights reserved.
//

extension UIColor {

    static let dullOrange =  UIColor(netHex:0xdcb13c)
    static let bluish = UIColor(netHex:0x2493a2)
    static let warmGrey = UIColor(netHex:0x707070)
    static let lightNavyBlue = UIColor(netHex:0x304a78)
    static let black = UIColor(netHex:0x131313)
    static let white = UIColor(netHex:0xffffff)
    static let camel = UIColor(netHex:0xbf9e46)

}

enum ColorThemeAgain: Float {
    
    case dullOrange = 1.1
    case bluish = 1.2
    case warmGrey = 1.3
    case lightNavyBlue = 1.4
    case black = 1.5
    case white = 1.6
    case camel = 1.7
        
    var color: UIColor {
        switch self {
        case .dullOrange: return UIColor.dullOrange
        case .bluish: return UIColor.bluish
        case .warmGrey: return UIColor.warmGrey
        case .lightNavyBlue: return UIColor.lightNavyBlue
        case .black: return UIColor.black
        case .white: return UIColor.white
        case .camel: return UIColor.camel
        }
    }
}

//struct Theme {
//    var background: UIColor
//    var text: UIColor
//    var tint: UIColor
//    var border: UIColor
//
//    init(background: UIColor = UIColor.clear, text: UIColor = UIColor.bluish, tint: UIColor = UIColor.blue, border: UIColor = UIColor.clear) {
//        self.background = background
//        self.text = text
//        self.tint = tint
//        self.border = border
//    }
//}
